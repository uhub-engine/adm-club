#!/bin/bash
# Ask the user for the folder to create host
# TODO: Not display the error when docker container is not running
echo 
echo
echo " ______     ______     __   __   __     ______        ";
echo "/\  __ \   /\  == \   /\ \ / /  /\ \   /\  ___\       ";
echo "\ \  __ \  \ \  __<   \ \ \'/   \ \ \  \ \___  \      ";
echo " \ \_\ \_\  \ \_\ \_\  \ \__|    \ \_\  \/\_____\     ";
echo "  \/_/\/_/   \/_/ /_/   \/_/      \/_/   \/_____/     ";
echo "                                                      ";
echo " _____     ______     ______     __  __     ______    ";
echo "/\  __-.  /\  __ \   /\  ___\   /\ \/ /    /\  ___\   ";
echo "\ \ \/\ \ \ \ \/\ \  \ \ \____  \ \  _'-.  \ \___  \  ";
echo " \ \____-  \ \_____\  \ \_____\  \ \_\ \_\  \/\_____\ ";
echo "  \/____/   \/_____/   \/_____/   \/_/\/_/   \/_____/ ";
echo "                                               v0.0.1    ";
echo
echo "Welcome to the Arvis Docks"
echo
echo "Lets create a simple virtualhost of nginx server"
echo "But first we need you tell us the foldername to send the trafic"
echo
echo "Current path: "
printf '\e[1;32m%-6s\e[m' "$(pwd)"
echo
echo
echo "Current directories: "
for i in $(ls)
do
  printf '\e[1;35m%-6s\e[m' " - ${i}" 
  echo 
done
echo
echo "path-to-folder : "
read varname
echo
echo "port : "
read port
echo
echo "container name : "
read container
echo
echo " Try to stop $container if is running: "
echo "$(docker stop $container)"
echo "$(docker rm $container)"
echo
printf '🐳🐳🐳 \e[0;36m%-6s\e[m 🐳🐳🐳' "Let's Dockerize"
echo
echo
printf ' Virtual Path to: \e[0;31m%-6s\e[m' "$(pwd)/${varname}" 
echo
echo
printf ' Server running: \e[1;32m%-6s\e[m' "http://localhost:$port"
echo
echo
printf ' Container: \e[0;36m%-6s\e[m' "$(docker run --name $container -p $port:80 -v $(pwd)/$varname:/usr/share/nginx/html -d nginx)"
echo
echo
