"use strict";
/*
 * Frontend Logic for application
 *
 */

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var endpoint = 'https://bapi.uhub.team';
var app = {
  encryption_key: endpoint.indexOf('localhost') > -1 ? 'ek_test_lF0W00Ds6hN45ZwAdQZQy2VZXSHHNU' : 'ek_live_MSc1djH6K72YHrVQW4guhnFm8wIugu'
};
app.ckeditor = {};
app.lists = {};
app.session = {};
app.institution = {};
app.counters = {
  dash: {}
};
app.counters.dash.bruteValue = 0;
app.counters.dash.liquidValue = 0;
app.counters.dash.congressValue = 0;
app.counters.dash.congressBrute = 0;
app.counters.dash.last7Value = 0;
app.counters.dash.last30Value = 0; // Other functions

app.addNewStyle = function (newStyle) {
  var styleElement = document.getElementById('styles_js');

  if (!styleElement) {
    styleElement = document.createElement('style');
    styleElement.type = 'text/css';
    styleElement.id = 'styles_js';
    document.getElementsByTagName('head')[0].appendChild(styleElement);
  }

  styleElement.appendChild(document.createTextNode(newStyle));
};

app.createUserPayment = function (uid) {
  try {
    var _loop = function _loop(index) {
      var user = app.lists.user[index];

      if (uid == user.id) {
        var mobile = typeof user.mobile == 'string' && user.mobile.length > 0 ? user.mobile : false;
        var name = typeof user.name == 'string' && user.name.length > 0 ? user.name : false;
        var email = typeof user.email == 'string' && user.email.length > 0 ? user.email : false;
        var cpf = typeof user.cpf == 'string' && user.cpf.length > 0 ? user.cpf.replace(/[^0-9]/g, "") : false;
        cpf = app.validateCPF(cpf) ? cpf : false;
        var address_state = typeof user.address_state == 'string' && user.address_state.length > 0 ? user.address_state : false;
        var address_city = typeof user.address_city == 'string' && user.address_city.length > 0 ? user.address_city : false;
        var address_neighborhood = typeof user.address_neighborhood == 'string' && user.address_neighborhood.length > 0 ? user.address_neighborhood : false;
        var address = typeof user.address == 'string' && user.address.length > 0 ? user.address : false;
        var address_number = typeof user.address_number == 'string' && user.address_number.length > 0 ? user.address_number : false;
        var postal_code = typeof user.postal_code == 'string' && user.postal_code.replace(/[^0-9]/g, "").length == 8 ? user.postal_code : false;

        if (!mobile || !name || !email || !cpf || !name || !address_state || !address_city || !address_neighborhood || !address || !address_number || !postal_code) {
          var e = new Error();
          e.details = JSON.stringify({
            mobile: mobile,
            name: name,
            email: email,
            cpf: cpf,
            address_state: address_state,
            address_city: address_city,
            address_neighborhood: address_neighborhood,
            address_complement: address_complement,
            address: address,
            address_number: address_number,
            postal_code: postal_code
          });
          throw e;
        }

        var listObject = {};
        var dataHtml = '';
        dataHtml += "<select name=\"isentationProduct\" id=\"isentationProduct\" class=\"custom-select mb-3\" required>";
        dataHtml += "<option selected>Selecione...</option>";

        for (var _index = 0; _index < app.lists.product.length; _index++) {
          var product = app.lists.product[_index];
          dataHtml += "<option value=\"".concat(product.id, "\">").concat(product.name, "</option>");
          listObject[product.id] = product;
        }

        dataHtml += "</select>";
        Swal.fire({
          title: '<strong>Pagamento Customizado</strong>',
          icon: 'warning',
          html: "\n            <div class=\"col-lg-12\">\n              <label for=\"isentationProduct\">Selecione para o que deseja criar um pagamento customizado</label>\n              ".concat(dataHtml, "\n              <div class=\"form-group\">\n                <label for=\"amountAdm\">Valor:</label>\n                <input type=\"number\" class=\"form-control\" id=\"amountAdm\"  aria-describedby=\"amountHelp\" placeholder=\"1000\">\n                <small id=\"amountHelp\" class=\"form-text text-muted\">O valor que deseja criar o pagamento deve seguir o seguinte modelo ex: R$ 10,00 = 1000.</small>\n              </div>\n            </div>\n          "),
          showCloseButton: true,
          showCancelButton: true,
          focusConfirm: false,
          confirmButtonText: '<i class="fa fa-thumbs-up"></i> Criar',
          confirmButtonAriaLabel: 'Thumbs up, add!',
          cancelButtonText: '<i class="fa fa-thumbs-down"></i>',
          cancelButtonAriaLabel: 'Thumbs down'
        }).then(function (e) {
          if (e.value) {
            $('.page-hover').fadeIn();
            var optionId = $("#isentationProduct option:selected").val();
            var amountAdm = $("#amountAdm").val();
            app.institution.congress = [{
              id: optionId,
              name: listObject[optionId].name,
              price: amountAdm
            }];
            var checkout = new PagarMeCheckout.Checkout({
              encryption_key: app.encryption_key,
              success: function success(data) {
                var payloadObj = _objectSpread({
                  access_token: app.getData('token'),
                  host: window.location.host,
                  productId: optionId,
                  customerId: uid,
                  amountAdm: amountAdm,
                  productTypeIsentation: listObject[optionId]['productTypeIsentation']
                }, data);

                app.client.request(undefined, endpoint + '/api/Admins/Institution/Customer/Payment', 'POST', undefined, payloadObj, function (statusCode, responsePayload) {
                  if (statusCode == 200 && responsePayload) {
                    if (responsePayload.payment_method == 'boleto') {
                      var boleto_url = responsePayload.boleto_url;
                      var boleto_barcode = responsePayload.boleto_barcode;
                      document.querySelector('.barcode-copy').innerText = boleto_barcode;
                      document.querySelector('.pdf-file').href = boleto_url;
                      $('#applicationModal').modal('show');
                    }

                    if (responsePayload.payment_method == 'pix') {
                      document.getElementById('qrcode').innerHTML = '';
                      document.querySelector('.pixcode-copy').innerText = '';
                      $('#qrcode').qrcode(responsePayload.tx.pix_qr_code);
                      $('#pixModal').modal('show');
                      document.querySelector('.pixcode-copy').innerText = responsePayload.tx.pix_qr_code;
                    }
                  }

                  $('.page-hover').fadeOut();
                });
              },
              error: function error(err) {
                console.log(err);
              },
              close: function close() {
                console.log('The modal has been closed.');
              }
            });
            var aPhoneNumbers = new Array();
            aPhoneNumbers.push("+55".concat(user.mobile.replace(/[^0-9]/g, "")));
            var customerData = {
              name: user.name,
              email: user.email,
              country: "br",
              external_id: user.email,
              documents: [{
                type: "cpf",
                number: user.cpf.replace(/[^0-9]/g, "")
              }],
              type: "individual",
              phone_numbers: aPhoneNumbers
            };
            var billingData = {
              name: user.name,
              address: {
                country: "br",
                state: user.address_state,
                city: user.address_city,
                neighborhood: user.address_neighborhood,
                complementary: user.address_complement,
                street: user.address,
                street_number: user.address_number,
                zipcode: user.postal_code.replace(/\D/gm, "")
              }
            };
            var iAmountPrice = 0;
            var aItems = [];
            var aPixItems = [];

            for (var _index2 = 0; _index2 < app.institution.congress.length; _index2++) {
              var oProduct = app.institution.congress[_index2];
              console.log(oProduct);
              var oProductTemplate = {
                id: oProduct.id,
                title: oProduct.name,
                unit_price: oProduct.price,
                quantity: 1,
                tangible: 'false'
              };
              iAmountPrice += oProduct.price;
              aItems.push(oProductTemplate);
              aPixItems.push({
                name: oProduct.name,
                value: "".concat(oProduct.price)
              });
            }

            var date = new Date();
            date.setDate(date.getDate() + 1);
            var pixExpirationDate = "".concat(date.getFullYear(), "-").concat(("0" + (date.getMonth() + 1)).slice(-2), "-").concat(("0" + date.getDate()).slice(-2));
            var pagarmeTemp = {
              amount: iAmountPrice,
              customerData: 'false',
              createToken: 'true',
              paymentMethods: 'boleto,pix',
              pix_expiration_date: pixExpirationDate,
              pix_additional_fields: aPixItems,
              boletoDiscountPercentage: 0,
              items: aItems,
              customer: customerData,
              billing: billingData
            };
            console.log(pagarmeTemp);
            checkout.open(pagarmeTemp);
          }
        });
      }
    };

    for (var index = 0; index < app.lists.user.length; index++) {
      _loop(index);
    }
  } catch (e) {
    var details = JSON.parse(e.details);
    var htmlData = '';
    htmlData += !details.mobile ? '<div class="card text-danger avaliation-card  col-6"><strong>Celular:</strong> Inválido</div><br>' : '<div class="card text-success avaliation-card col-6"><strong>Celular:</strong> Ok</div><br>';
    htmlData += !details.name ? '<div class="card text-danger avaliation-card  col-6"><strong>Nome:</strong> Inválido</div><br>' : '<div class="card text-success avaliation-card col-6"><strong>Nome:</strong> Ok</div><br>';
    htmlData += !details.email ? '<div class="card text-danger avaliation-card  col-6"><strong>e-mail:</strong> Inválido</div><br>' : '<div class="card text-success avaliation-card col-6"><strong>e-mail:</strong> Ok</div><br>';
    htmlData += !details.cpf ? '<div class="card text-danger avaliation-card  col-6"><strong>CPF:</strong> Inválido</div><br>' : '<div class="card text-success avaliation-card col-6"><strong>CPF:</strong> Ok</div><br>';
    htmlData += !details.address_state ? '<div class="card text-danger avaliation-card  col-6"><strong>Estado:</strong> Inválido</div><br>' : '<div class="card text-success avaliation-card col-6"><strong>Estado:</strong> Ok</div><br>';
    htmlData += !details.address_city ? '<div class="card text-danger avaliation-card  col-6"><strong>Cidade:</strong> Inválido</div><br>' : '<div class="card text-success avaliation-card col-6"><strong>Cidade:</strong> Ok</div><br>';
    htmlData += !details.address_neighborhood ? '<div class="card text-danger avaliation-card  col-6"><strong>Bairro:</strong> Inválido</div><br>' : '<div class="card text-success avaliation-card col-6"><strong>Bairro:</strong> Ok</div><br>';
    htmlData += !details.address ? '<div class="card text-danger avaliation-card  col-6"><strong>Endereço:</strong> Inválido</div><br>' : '<div class="card text-success avaliation-card col-6"><strong>Endereço:</strong> Ok</div><br>';
    htmlData += !details.address_number ? '<div class="card text-danger avaliation-card no-border-bottom col-6"><strong>Número:</strong> Inválido</div><br>' : '<div class="card text-success avaliation-card no-border-bottom col-6"><strong>Número:</strong> Ok</div><br>';
    htmlData += !details.postal_code ? '<div class="card text-danger avaliation-card no-border-bottom col-6"><strong>CEP:</strong> Inválido</div><br>' : '<div class="card text-success avaliation-card no-border-bottom col-6"><strong>CEP:</strong> Ok</div><br>';
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      html: "<p class=\"text-left\">Abaixo listamos quais dados voc\xEA precisa corrigir para que possa criar um pagamento.<br><br></p><div class=\"row\">".concat(htmlData, "</div>")
    });
  }
};

app.printDiv = function (divName) {
  var documentPrintable = document.getElementById(divName);
  var documentChield = documentPrintable.children[0];
  documentChield.classList.add("w-100");
  var printContents = document.getElementById(divName).innerHTML;
  var originalContents = document.body.innerHTML;
  document.body.innerHTML = "\n     <div class=\"d-flex flex-column h-100 w-100 p-3\">\n      <h1 class=\"text-center\">\n          Comprovante de pagamento.\n      </h1>\n      <div class=\"mt-3 mb-5\">\n          <p class=\"d-flex justify-content-center\">Comprovante de que usu\xE1rio realizou o pagamento dos items abaixo por meio da plataforma da Uhub</p>\n      </div>\n      ".concat(printContents, "\n     </div>\n  ");
  window.print();
  document.body.innerHTML = originalContents;
  documentChield.classList.remove("w-100");
};

app.deleteTx = function (transactionId) {
  app["delete"](transactionId, 'Transaction');
};

app.loadUserDataModal = function (modalId, userId) {
  for (var index = 0; index < app.lists.user.length; index++) {
    var oUser = app.lists.user[index];

    if (oUser.id == userId) {
      var dBirthday;
      var sHtmlTemplate;

      (function () {
        dBirthday = new Date(oUser.birthday);
        dBirthday.setDate(dBirthday.getDate() + 1);
        document.querySelector('#userIdDetailModal').value = oUser.id || '';
        document.querySelector('#nameUser').value = oUser.name || '';
        document.querySelector('#userEmail').value = oUser.email || '';
        document.querySelector('#gender').value = oUser.gender || '';
        document.querySelector('#birthday').value = "".concat(dBirthday.getFullYear().toString(), "-").concat((dBirthday.getMonth() + 1).toString().padStart(2, 0), "-").concat(dBirthday.getDate().toString().padStart(2, 0)) || '';
        document.querySelector('#cpf').value = oUser.cpf || '';
        document.querySelector('#rg').value = oUser.rg || '';
        document.querySelector('#phone').value = oUser.phone || '';
        document.querySelector('#mobile').value = oUser.mobile || '';
        document.querySelector('#address').value = oUser.address || '';
        document.querySelector('#address_number').value = oUser.address_number || '';
        document.querySelector('#address_complement').value = oUser.address_complement || '';
        document.querySelector('#address_neighborhood').value = oUser.address_neighborhood || '';
        document.querySelector('#address_city').value = oUser.address_city || '';
        document.querySelector('#address_state').value = oUser.address_state || '';
        document.querySelector('#address_country').value = oUser.address_country || '';
        document.querySelector('#postal_code').value = oUser.postal_code || '';
        document.querySelector('#role').value = oUser.role || '';
        document.querySelector('#institution_abreviation').value = oUser.institution_abreviation || '';
        document.querySelector('#professional_phone').value = oUser.professional_phone || '';
        document.querySelector('#professional_address').value = oUser.professional_address || '';
        document.querySelector('#professional_address_complement').value = oUser.professional_address_complement || '';
        document.querySelector('#professional_address_neighborhood').value = oUser.professional_address_neighborhood || '';
        document.querySelector('#professional_address_city').value = oUser.professional_address_city || '';
        document.querySelector('#professional_address_state').value = oUser.professional_address_state || '';
        document.querySelector('#professional_address_country').value = oUser.professional_address_country || '';
        document.querySelector('#professional_postal_code').value = oUser.professional_postal_code || '';
        document.querySelector('#title').value = oUser.title || '';
        document.querySelector('#degree_institution').value = oUser.degree_institution || '';
        document.querySelector('#degree_course').value = oUser.degree_course || '';
        document.querySelector('#year_conclusion_degree').value = oUser.year_conclusion_degree || '';
        document.querySelector('#specialization_institution').value = oUser.specialization_institution || '';
        document.querySelector('#specialization_course').value = oUser.specialization_course || '';
        document.querySelector('#year_conclusion_specialization').value = oUser.year_conclusion_specialization || '';
        document.querySelector('#master_institution').value = oUser.master_institution || '';
        document.querySelector('#master_course').value = oUser.master_course || '';
        document.querySelector('#year_conclusion_master').value = oUser.year_conclusion_master || '';
        document.querySelector('#doc_institution').value = oUser.doc_institution || '';
        document.querySelector('#doc_course').value = oUser.doc_course || '';
        document.querySelector('#year_conclusion_doc').value = oUser.year_conclusion_doc || '';
        document.querySelector('#phd_institution').value = oUser.phd_institution || '';
        document.querySelector('#phd_course').value = oUser.phd_course || '';
        document.querySelector('#year_conclusion_phd').value = oUser.year_conclusion_phd || '';
        sHtmlTemplate = '';
        var oStatus = {
          paid: 'Pago',
          waiting_payment: "Aguardando Pagamento",
          platform_include: "Pagamento Antecipado",
          refunded: "Estornado"
        };
        var colorTheme = {
          "waiting_payment": "bg-warning text-white",
          "paid": "bg-success text-white"
        };
        var iconTheme = {
          platform_include: "<i class=\"fad fa-cash-register\" ></i>",
          boleto: "<i class=\"fad fa-file-invoice-dollar\"></i>",
          credit_card: "<i class=\"fad fa-credit-card\"></i>",
          pix: "<i class=\"fab fa-xing\"></i>"
        }; // oUser.transactions.reverse();

        oUser.transactions.forEach(function (row) {
          var buttonDelete = '';
          var buttonBoleto = '';
          var buttonHtml = '';

          if (row.type == 'boleto' && row.status == 'waiting_payment') {
            buttonBoleto = "<a href=\"".concat(row.tx.boleto_url, "\" target=\"_blank\">Visualizar Boleto</a><br>");
          }

          if (row.status == 'waiting_payment') {
            buttonDelete = "\n              <button type=\"button\" class=\"btn btn-danger\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Deleter esta transa\xE7\xE3o que ainda n\xE3o foi finalizada.\" onclick=\"app.deleteTx('".concat(row.id, "')\">\n                <i class=\"fad fa-trash\"></i>\n              </button>\n            ");
          }

          var historyContentIcon = "\n              <div class=\"tracking-icon status-inforeceived ".concat(colorTheme[row.status], "\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"").concat(typeof row.madeBy == 'string' && row.madeBy.length > 0 ? 'Transação de Isenção' : 'Transação anterior a Uhub', "\" >\n                  ").concat(typeof row.madeBy == 'string' && row.madeBy.length > 0 ? '<i class="fab fa-acquisitions-incorporated"></i>' : iconTheme[row.type], "\n              </div>\n          ");
          var historyContentBody = "\n              <div class=\"tracking-date\">".concat(new Date(row.date).toLocaleString(), "</div>\n              <div class=\"tracking-content\">\n                  <div class=\"card\">\n                      <div class=\"card-header d-flex flex-row justify-content-between\">\n                          <strong><i><small>#").concat(row.id, "</small></i></strong>\n                          <button type=\"button\" class=\"btn btn-secondary\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Transa\xE7\xF5es anteriores a Uhub servem apenas como hist\xF3rico e n\xE3o possuem comprovante.\"/><i class=\"fad fa-print\"></i> Imprimir Comprovante</button>\n                          ").concat(buttonDelete, "\n                      </div>\n                      <div class=\"card-body\">\n                          <table class=\"table table-hover table-bordered\">\n                              <thead>\n                                  <tr>\n                                      <th scope=\"col\">Item</th>\n                                      <th scope=\"col\">Valor</th>\n                                  </tr>\n                              </thead>\n                              <tbody>\n                                  <tr>\n                                      <td>").concat(row.ref || "Pagamento inclu\xEDdo via plataforma. ".concat(new Date(row.date).getFullYear()), "</td>\n                                      <td>R$ ").concat((row.amount / 100).toLocaleString('pt-BR', {
            minimumFractionDigits: 2,
            maximumFractionDigits: 3
          }), "</td>\n                                  </tr>\n                              </tbody>\n                          </table>\n                      </div>\n                      <div class=\"card-footer d-flex flex-row justify-content-between\">\n                          <p>\n                              <strong>").concat(buttonBoleto, " Status:</strong> ").concat(typeof row.madeBy == 'string' && row.madeBy.length > 0 ? 'Isento' : oStatus[row.status], "<br>\n                          </p>\n                          <p>\n                              <strong>Valor:</strong> R$ ").concat(typeof row.madeBy == 'string' && row.madeBy.length > 0 ? 0 : (row.amount / 100).toLocaleString('pt-BR', {
            minimumFractionDigits: 2,
            maximumFractionDigits: 3
          }), "<br>\n                          </p>\n                      </div>\n                  </div>\n              </div>\n              \n          ");

          if (row.type != 'platform_include') {
            historyContentIcon = "\n                  <div class=\"tracking-icon status-inforeceived ".concat(colorTheme[row.status], "\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Transa\xE7\xE3o realizada com a Uhub\">\n                      ").concat(iconTheme[row.type], "\n                  </div>\n              ");
            var itemContent = '';

            for (var _index3 = 0; _index3 < row.tx.items.length; _index3++) {
              var oItem = row.tx.items[_index3];
              itemContent += "\n                      <tr>\n                          <td>".concat(oItem.title, "</td>\n                          <td>R$ ").concat((oItem.unit_price / 100).toLocaleString('pt-BR', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 3
              }), "</td>\n                      </tr>\n                  ");
            }

            if (buttonBoleto.length < 1) {
              buttonHtml = "<button type=\"button\" class=\"btn btn-secondary\" disabled/><i class=\"fad fa-print\"></i> Imprimir Comprovante</button>";
            } else {
              buttonHtml = "<a href=\"".concat(row.tx.boleto_url, "\" target=\"_blank\" class=\"btn btn-primary\"><i class=\"fad fa-file-invoice-dollar\"></i> Visualizar Boleto</a> ");
            }

            historyContentBody = "\n                  <div class=\"tracking-date\">".concat(new Date(row.date).toLocaleString(), "</div>\n                  <div class=\"tracking-content\">\n                      <div id=\"printable-").concat(row.id, "\">\n                          \n                          <div class=\"card mb-3\">\n                              <div class=\"card-header d-flex flex-row justify-content-between\">\n                                  <strong><i><small>#").concat(row.id, "</small></i></strong>\n                                  ").concat(buttonHtml, "\n                                  ").concat(buttonDelete, "\n                              </div>\n                              <div class=\"card-body\">\n                                  <table class=\"table table-hover table-bordered\">\n                                      <thead>\n                                          <tr>\n                                              <th scope=\"col\">Item</th>\n                                              <th scope=\"col\">Valor</th>\n                                          </tr>\n                                      </thead>\n                                      <tbody>\n                                          ").concat(itemContent, "\n                                      </tbody>\n                                  </table>\n                              </div>\n                              <div class=\"card-footer d-flex flex-row justify-content-between\">\n                                  <p>\n                                      <strong>Status:</strong> ").concat(oStatus[row.status], "<br>\n                                  </p>\n                                  <p>\n                                      <strong>Valor:</strong> R$ ").concat((row.amount / 100).toLocaleString('pt-BR', {
              minimumFractionDigits: 2,
              maximumFractionDigits: 3
            }), "<br>\n                                  </p>\n                              </div>\n                          </div>\n                      </div>\n                      \n                  </div>\n                  \n              "); // Exibir o comprovante pois já o deve possuir.

            console.log();
          }

          sHtmlTemplate += "\n              <div class=\"tracking-item\">\n                  ".concat(historyContentIcon, "\n                  ").concat(historyContentBody, "\n              </div>");
        });
        document.getElementById('historyTransaction').innerHTML = sHtmlTemplate;
        $('[data-toggle="tooltip"]').tooltip();
        $("#".concat(modalId)).modal('show');
      })();
    }
  }
};

app.loadWorkDataModal = function (modalId, objIndex) {
  var oWork = app.lists.workList[objIndex];
  console.log(modalId, oWork);
};

app.loadAvaliatorStatusChange = function (congressId, avaliatorId, status) {
  app.congressId = congressId;
  $('.page-hover').fadeIn(1000);
  var token = typeof app.user.token == 'string' ? app.user.token : false;

  if (token) {
    var requestOb = {
      'host': window.location.host,
      'access_token': token,
      'congressId': congressId,
      'avaliatorId': avaliatorId,
      'status': status
    };
    app.client.request(undefined, endpoint + '/api/Admins/Institution/Congress/Work/Avaliator', 'POST', undefined, requestOb, function (statusCode, responsePayload) {
      if (statusCode == 200) {
        Swal.fire('Uhu', 'Avaliador alterado com sucesso.', 'success').then(function (e) {
          app.loadEventsPage(function (r) {
            console.log(r);
            app.loadCongressDataModal('congress-detail-modal', app.congressId);
          });
        });
      }
    });
  }
};

app.loadBookDetails = function (modalId, objIndex) {
  var oBook = app.lists.booksByKey[objIndex];
  document.getElementById('nameBookDetail').value = oBook.name;
  document.getElementById('areaBookDetail').value = oBook.area;
  document.getElementById('typeBookDetail').value = oBook.type;
  document.getElementById('editorBookDetail').value = oBook.editor;
  document.getElementById('coauthorBookDetail').value = oBook.coauthor;
  document.getElementById('linkBookDetail').value = oBook.link;
  document.getElementById('linkSubmitionBookDetail').value = oBook.linkSubmition;
  document.getElementById('resumeBookDetail').innerHTML = oBook.resume;
  $("#".concat(modalId)).modal('show'); // console.log(modalId, oBook)
};

app.loadBookStatusChange = function (congressId, bookId, status) {
  app.congressId = congressId;
  $('.page-hover').fadeIn(1000);
  var token = typeof app.user.token == 'string' ? app.user.token : false;

  if (token) {
    var requestOb = {
      'host': window.location.host,
      'access_token': token,
      congressId: congressId,
      bookId: bookId,
      status: status
    };
    app.client.request(undefined, endpoint + '/api/Admins/Institution/Congress/Publicom', 'POST', undefined, requestOb, function (statusCode, responsePayload) {
      if (statusCode == 200) {
        Swal.fire('Uhu', 'Livro alterado com sucesso.', 'success').then(function (e) {
          app.loadEventsPage(function (r) {
            document.location.reload();
          });
        });
      }
    });
  }
};

app.setAvaliatorToWork = function (congressId, workId) {
  var dataHtml = '';

  for (var index = 1; index <= 3; index++) {
    dataHtml += "<select name=\"avalWork-".concat(index, "\" id=\"avalWork-").concat(index, "\" class=\"custom-select mb-3\" >");
    dataHtml += "<option selected>Selecione...</option>";

    for (var _index4 = 0; _index4 < app.lists.avaliatorList.length; _index4++) {
      var avaliator = app.lists.avaliatorList[_index4];
      dataHtml += "<option value=\"".concat(avaliator.id, "\">").concat(avaliator.ies, " - ").concat(avaliator.customer.name, "</option>");
    }

    dataHtml += "</select>";
  }

  Swal.fire({
    title: '<strong>Adicionar Avaliador</strong>',
    icon: 'info',
    html: "\n      <div class=\"col-lg-12\">\n        <label for=\"avalWork\">Selecione o convidado</label>\n        ".concat(dataHtml, "\n      </div>\n    "),
    showCloseButton: true,
    showCancelButton: true,
    focusConfirm: false,
    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Adicionar',
    confirmButtonAriaLabel: 'Thumbs up, add!',
    cancelButtonText: '<i class="fa fa-thumbs-down"></i>',
    cancelButtonAriaLabel: 'Thumbs down'
  }).then(function (e) {
    if (e.value) {
      var token = typeof app.user.token == 'string' ? app.user.token : false;

      if (token) {
        var payloadObj = {
          'host': window.location.host,
          'access_token': token,
          'congressId': congressId,
          'avaliatorId1': $("#avalWork-1 option:selected").val(),
          'avaliatorId2': $("#avalWork-2 option:selected").val(),
          'avaliatorId3': $("#avalWork-3 option:selected").val(),
          'workId': workId
        };
        app.client.request(undefined, endpoint + '/api/Admins/Institution/Congress/Work/Aval', 'POST', undefined, payloadObj, function (statusCode, responsePayload) {
          if (statusCode == 200) {
            Swal.fire('Uhu', 'Avaliador adicionado com sucesso.', 'success').then(function (e) {// if(e.value){
              //   window.location.reload();
              // } 
            });
          }
        });
      }

      console.log($("#avalWork option:selected").text());
    }
  });
};

app.parseAvaliatorInformationFromLists = function (aAvaliatorsData) {
  var aAvaliators = [];

  for (var index = 0; index < app.lists.avaliatorList.length; index++) {
    var oAvaliator = app.lists.avaliatorList[index];
    console.log(oAvaliator, aAvaliatorsData, aAvaliatorsData.indexOf(oAvaliator.customerId));

    if (aAvaliatorsData.indexOf(oAvaliator.customerId) > -1) {
      aAvaliators.push(oAvaliator);
    }
  }

  return aAvaliators;
};

app.deleteAvaliator = function (itemClass, customerId, workId) {
  Swal.fire({
    title: 'Atenção',
    text: "Você realmente deseja remover o avalidor deste trabalho",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Sim',
    cancelButtonText: 'Não'
  }).then(function (result) {
    if (result.value) {
      // Get the token from the current sessionToken, or log the user out if none is there
      var token = typeof app.user.token == 'string' ? app.user.token : false;

      if (token) {
        // Fetch the user data
        var queryStringObject = {
          'host': window.location.host,
          'access_token': token,
          workId: workId,
          avaliatorId: customerId
        };
        app.client.request(undefined, endpoint + '/api/Admins/Institution/Congress/Work/Avaliator', 'DELETE', queryStringObject, undefined, function (statusCode, responsePayload) {
          if (statusCode == 200 && responsePayload) {
            Swal.fire('Uhu', 'Registro deletado com sucesso.', 'success').then(function (e) {
              if (e.value) {
                document.querySelector(".".concat(itemClass)).remove();
              }
            });
          }
        });
      }
    }
  });
};

app.deleteBook = function (congressId, bookId) {
  Swal.fire({
    title: 'Atenção',
    text: "Você realmente deseja deletar está submissão de livro?",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Sim',
    cancelButtonText: 'Não'
  }).then(function (result) {
    if (result.value) {
      // Get the token from the current sessionToken, or log the user out if none is there
      var token = typeof app.user.token == 'string' ? app.user.token : false;

      if (token) {
        // Fetch the user data
        var queryStringObject = {
          'host': window.location.host,
          'access_token': token,
          congressId: congressId,
          bookId: bookId
        };
        app.client.request(undefined, endpoint + '/api/Admins/Institution/Congress/Publicom', 'DELETE', queryStringObject, undefined, function (statusCode, responsePayload) {
          if (statusCode == 200 && responsePayload) {
            Swal.fire('Uhu', 'Registro deletado com sucesso.', 'success').then(function (e) {
              if (e.value) {
                document.querySelector(".".concat(itemClass)).remove();
              }
            });
          }
        });
      }
    }
  });
};

app.listAvaliatorsOfWork = function (congressId, workId) {
  for (var index = 0; index < app.lists.workList.length; index++) {
    var oWork = app.lists.workList[index];

    if (oWork.id == workId) {
      var showAvaliatorsHtml = '';

      if (_typeof(oWork.avaliators) == 'object' && oWork.avaliators instanceof Array && oWork.avaliators.length > 0) {
        var aAvaliators = app.parseAvaliatorInformationFromLists(oWork.avaliators);
        showAvaliatorsHtml += '<ul class="list-group">';

        for (var _index5 = 0; _index5 < aAvaliators.length; _index5++) {
          var oAvaliator = aAvaliators[_index5];
          showAvaliatorsHtml += "\n            <li class=\"list-group-item item-".concat(_index5, "\">\n              <strong>").concat(oAvaliator.customer.name, "</strong>  \n              <button type=\"button\" class=\"btn btn-danger\" onClick=\"app.deleteAvaliator('item-").concat(_index5, "','").concat(oAvaliator.customer.id, "','").concat(workId, "')\">\n                <i class=\"fas fa-user-minus\"></i>\n              </button>\n            </li>");
        }

        showAvaliatorsHtml += '</ul>';
        Swal.fire({
          title: 'Avaliadores',
          icon: '',
          html: showAvaliatorsHtml,
          showCloseButton: false,
          showCancelButton: false,
          showConfirmButton: false,
          focusConfirm: false
        });
      } else {
        showAvaliatorsHtml = 'No momento este trabalho não possui avaliadores.';
        Swal.fire({
          icon: 'warning',
          html: showAvaliatorsHtml,
          showCloseButton: false,
          showCancelButton: false,
          showConfirmButton: false,
          focusConfirm: false
        });
      }
    }
  }
};

app.setFinishToWork = function () {
  Swal.fire({
    title: '<strong>Finalizar Trabalho</strong>',
    icon: 'info',
    html: "\n      <div class=\"col-lg-12\">\n        <label for=\"finalNote\">Nota final do trabalho</label>\n        <input type=\"number\" min=\"0\" max=\"10\" step=\"0.5\" class=\"form-control\" name=\"finalNote\" id=\"finalNote\" placeholder=\"Preenchar para finalizar a analize\">\n      </div>\n    ",
    showCloseButton: true,
    showCancelButton: true,
    focusConfirm: false,
    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Adicionar',
    confirmButtonAriaLabel: 'Thumbs up, add!',
    cancelButtonText: '<i class="fa fa-thumbs-down"></i>',
    cancelButtonAriaLabel: 'Thumbs down'
  });
};

app.bindCordinatorButton = function () {
  document.querySelector('.add-new-coordinator').addEventListener('click', function (e) {
    e.preventDefault();
    console.log(app.lists.userList);
    var congressId = app.congress.id;
    var dataHtml = '';

    for (var index = 1; index <= 3; index++) {
      dataHtml += "<select name=\"avalWork-".concat(index, "\" id=\"avalWork-").concat(index, "\" class=\"custom-select mb-3\" >");
      dataHtml += "<option selected>Selecione...</option>";

      for (var _index6 = 0; _index6 < app.lists.userList.length; _index6++) {
        var cordinator = app.lists.userList[_index6];
        dataHtml += "<option value=\"".concat(cordinator.id, "\">").concat(cordinator.name, "</option>");
      }

      dataHtml += "</select>";
    }

    dataHtml += '<hr>';
    dataHtml += '<span>Categoria</span>';
    dataHtml += '<div class="d-flex-row justify-content-between mt-3">';

    for (var _index7 = 0; _index7 < app.lists.categList.length; _index7++) {
      var category = app.lists.categList[_index7];
      dataHtml += "\n        <div class=\"form-check form-check-inline col-12\">\n          <input class=\"form-check-input checkbox-categ-cordinator\" type=\"checkbox\" id=\"".concat(category.replaceAll(" ", ""), "\" value=\"").concat(category, "\">\n          <label class=\"form-check-label\" for=\"").concat(category.replaceAll(" ", ""), "\">").concat(category, "</label>\n        </div>\n      ");
    }

    dataHtml += '</div>';
    Swal.fire({
      title: '<strong>Adicionar Coordenador(es)</strong>',
      icon: 'info',
      html: "\n        <div class=\"col-lg-12\">\n          <label for=\"avalWork\">Selecione o coordenador</label>\n          ".concat(dataHtml, "\n        </div>\n      "),
      showCloseButton: true,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText: '<i class="fa fa-thumbs-up"></i> Adicionar',
      confirmButtonAriaLabel: 'Thumbs up, add!',
      cancelButtonText: '<i class="fa fa-thumbs-down"></i>',
      cancelButtonAriaLabel: 'Thumbs down'
    }).then(function (e) {
      if (e.value) {
        var categCheck = [];
        var categories = $('.checkbox-categ-cordinator:checkbox:checked');

        for (var _index8 = 0; _index8 < categories.length; _index8++) {
          var categ = categories[_index8];
          categCheck.push(categ.value);
        }

        var token = typeof app.user.token == 'string' ? app.user.token : false;

        if (token) {
          var payloadObj = {
            'host': window.location.host,
            'access_token': token,
            'congressId': congressId,
            'cordinator[1]': $("#avalWork-1 option:selected").val(),
            'cordinator[2]': $("#avalWork-2 option:selected").val(),
            'cordinator[3]': $("#avalWork-3 option:selected").val(),
            'categs': categCheck
          };
          app.client.request(undefined, endpoint + '/api/Admins/Institution/Congress/Work/Cordinator', 'POST', undefined, payloadObj, function (statusCode, responsePayload) {
            if (statusCode == 200) {
              Swal.fire('Uhu', 'Coordenador(es) adicionado(s) com sucesso.', 'success').then(function (e) {// if(e.value){
                //   window.location.reload();
                // } 
              });
            }
          });
        }
      }
    });
    dataHtml += '';
  });
};

app.loadCordinatorDetail = function (cordinatorId) {
  cordinatorId = typeof cordinatorId == 'string' && cordinatorId.length > 0 ? cordinatorId : false;
  var cordinators = _typeof(app.lists.cordinatorsList) == 'object' && app.lists.cordinatorsList.length > 0 ? app.lists.cordinatorsList : false;

  if (cordinatorId && cordinators) {
    var _loop2 = function _loop2(index) {
      var cordinator = cordinators[index];

      if (cordinator.id == cordinatorId) {
        congressId = app.congress.id;
        var dataHtml = '';
        dataHtml += '<span>Categoria</span>';
        dataHtml += '<div class="d-flex-row justify-content-between mt-3">';

        for (var _index9 = 0; _index9 < app.lists.categList.length; _index9++) {
          var category = app.lists.categList[_index9];
          dataHtml += "\n            <div class=\"form-check form-check-inline col-6\">\n              <input class=\"form-check-input checkbox-categ-cordinator\" type=\"checkbox\" id=\"".concat(category.replaceAll(" ", ""), "\" value=\"").concat(category, "\" ").concat(cordinator.categs.indexOf(category) > -1 ? 'checked' : '', ">\n              <label class=\"form-check-label\" for=\"").concat(category.replaceAll(" ", ""), "\">").concat(category, "</label>\n            </div>\n          ");
        }

        dataHtml += '</div>';
        Swal.fire({
          title: '<strong>Editar Coordenador</strong>',
          icon: 'info',
          html: "\n            <div class=\"col-lg-12\">\n              ".concat(dataHtml, "\n            </div>\n          "),
          showCloseButton: true,
          showCancelButton: true,
          focusConfirm: false,
          confirmButtonText: '<i class="fa fa-thumbs-up"></i> Atualizar',
          confirmButtonAriaLabel: 'Thumbs up, add!',
          cancelButtonText: '<i class="fa fa-thumbs-down"></i>',
          cancelButtonAriaLabel: 'Thumbs down'
        }).then(function (e) {
          if (e.value) {
            var categCheck = [];
            var categories = $('.checkbox-categ-cordinator:checkbox:checked');

            for (var _index10 = 0; _index10 < categories.length; _index10++) {
              var categ = categories[_index10];
              categCheck.push(categ.value);
            }

            var token = typeof app.user.token == 'string' ? app.user.token : false;

            if (token) {
              var payloadObj = {
                'host': window.location.host,
                'access_token': token,
                'congressId': congressId,
                'categs': categCheck,
                'cordinator': cordinator.customerId
              };
              app.client.request(undefined, endpoint + '/api/Admins/Institution/Congress/Work/Cordinator', 'POST', undefined, payloadObj, function (statusCode, responsePayload) {
                if (statusCode == 200) {
                  Swal.fire('Uhu', 'Coordenador atualizado com sucesso.', 'success').then(function (e) {// if(e.value){
                    //   window.location.reload();
                    // } 
                  });
                }
              });
            }
          }
        });
      }
    };

    for (var index = 0; index < cordinators.length; index++) {
      var congressId;

      _loop2(index);
    }
  }
};

app.viewAvaliation = function _callee(congressId, workId) {
  var data, index, dataHtml, avaliatorsFinal, avaliationPoints, divider, avaliationsByAvaliator, _index11, _avaliation, key, avaliator, avaliation, _key, media;

  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          console.log(congressId, workId);
          data = {};

          for (index = 0; index < app.lists.workList.length; index++) {
            if (workId == app.lists.workList[index].id) {
              data = app.lists.workList[index].avaliations;
            }
          }

          dataHtml = '';
          avaliatorsFinal = [];
          avaliationPoints = {
            coerenceOfContent: 0,
            experimentalismOfProduct: 0,
            productQuality: 0
          };
          divider = 0;
          avaliationsByAvaliator = {};

          for (_index11 = 0; _index11 < data.length; _index11++) {
            _avaliation = data[_index11];

            if (_typeof(avaliationsByAvaliator[_avaliation.customerId]) != 'object') {
              avaliationsByAvaliator[_avaliation.customerId] = [];
            }

            avaliationsByAvaliator[_avaliation.customerId].push(_avaliation);
          }

          _context.t0 = regeneratorRuntime.keys(avaliationsByAvaliator);

        case 10:
          if ((_context.t1 = _context.t0()).done) {
            _context.next = 24;
            break;
          }

          key = _context.t1.value;

          if (!Object.hasOwnProperty.call(avaliationsByAvaliator, key)) {
            _context.next = 22;
            break;
          }

          avaliator = avaliationsByAvaliator[key];
          _context.next = 16;
          return regeneratorRuntime.awrap(avaliator.reduce(function (a, b) {
            return new Date(a.date) > new Date(b.date) ? a : b;
          }));

        case 16:
          avaliation = _context.sent;
          avaliationPoints.coerenceOfContent += +avaliation.coerenceOfContent;
          avaliationPoints.experimentalismOfProduct += +avaliation.experimentalismOfProduct;
          avaliationPoints.productQuality += +avaliation.productQuality;
          divider++;
          dataHtml += "\n              <div class=\"list-group-item list-group-item-action flex-column align-items-start\">\n                  <div class=\"d-flex w-100 justify-content-between\">\n                      <p class=\"mb-1\"><i class=\"fad fa-clipboard-list-check\"></i> ".concat(avaliation.customer.name, "</p>\n                      \n                  </div>\n                  <hr>\n                  <small class=\"mb-1 text-left\">\n                      <strong>O experimentalismo do produto:</strong> ").concat(avaliation.experimentalismOfProduct, "<br>\n                      <strong>A qualidade t\xE9cnica do produto:</strong> ").concat(avaliation.productQuality, "<br>\n                      <strong>A consist\xEAncia te\xF3rica e coer\xEAncia do conte\xFAdo inserido no formul\xE1rio-padr\xE3o com o respectivo produto:</strong> ").concat(avaliation.coerenceOfContent, "<br>\n                  </small>\n                  <hr>\n                  <small class=\"text-muted text-bold\">Observa\xE7\xF5es:</small>\n                  <div class=\"container\">\n                      ").concat(avaliation.descriptionAvaliationWorkAvaliator, "\n                  </div>\n                  <hr>\n                  <small class=\"text-muted\">").concat(new Date(avaliation.date).toLocaleString(), "</small>\n              </div>\n          ");

        case 22:
          _context.next = 10;
          break;

        case 24:
          for (_key in avaliationPoints) {
            if (Object.hasOwnProperty.call(avaliationPoints, _key)) {
              avaliationPoints[_key] = avaliationPoints[_key] / divider;
            }
          }

          media = (avaliationPoints.coerenceOfContent + avaliationPoints.experimentalismOfProduct + avaliationPoints.productQuality) / 3;
          Swal.fire({
            title: '<strong>Avaliações</strong>',
            icon: 'info',
            html: "\n    <p>M\xE9dia de nota: ".concat(media, "</p>\n    <div class=\"col-lg-12\">\n      ").concat(dataHtml, "\n    </div>\n  "),
            showCloseButton: false,
            showCancelButton: false,
            focusConfirm: false
          });

        case 27:
        case "end":
          return _context.stop();
      }
    }
  });
};

app.loadIndicationDetail = function (workId, congressId) {
  var work = '';

  for (var index = 0; index < app.lists.indications.length; index++) {
    if (app.lists.indications[index].id == workId) {
      work = app.lists.indications[index];
    }
  }

  var congress = '';

  for (var _index12 = 0; _index12 < app.lists.congressList.length; _index12++) {
    if (app.lists.congressList[_index12].id == congressId) {
      congress = app.lists.congressList[_index12];
    }
  }

  $('#inscriptionWorkModal').modal('show');
  console.log(congress);
  console.log(work);
  document.getElementById('eventHost').value = window.location.host;
  document.getElementById('congressIdWorkSub').value = congress.id;
  document.getElementById('workIdWorkSub').value = work.id;
  document.getElementById('workNameWorkModal').value = work.workName;
  document.getElementById('nameWorkModal').value = work.name;
  document.getElementById('roleiesWorkModal').value = work.roleies;
  document.getElementById('cpfWorkModal').value = work.cpf;
  document.getElementById('mobileWorkModal').value = work.mobile;
  document.getElementById('emailWorkModal').value = work.email;
  document.getElementById('leaderNameWorkModal').value = work.leaderName;
  document.getElementById('leaderDocWorkModal').value = work.leaderDoc; // Creating the entire select list of ies to select after

  if (_typeof(congress.iesList) == 'object' && congress.iesList instanceof Array && congress.iesList.length > 0) {
    var ieSelect = document.getElementById('iesWorkModal');

    for (var _index13 = 0; _index13 < congress.iesList.length; _index13++) {
      var ies = congress.iesList[_index13];
      ieSelect.options[ieSelect.options.length] = new Option(ies.name, ies.name);
    }
  }

  document.getElementById('iesWorkModal').value = work.ies; // Creating the entire select list o categories to select after

  if (_typeof(congress.categories) == 'object' && congress.categories instanceof Array && congress.categories.length > 0) {
    var categorySelect = document.getElementById('categoryWorkModal');

    for (var _index14 = 0; _index14 < congress.categories.length; _index14++) {
      var oCategory = congress.categories[_index14];
      categorySelect.options[categorySelect.options.length] = new Option(oCategory, oCategory);
    }
  }

  document.getElementById('categoryWorkModal').value = work.category;
  $(document).on('show.bs.modal', '.modal', function (event) {
    var zIndex = 1040 + 10 * $('.modal:visible').length;
    $(this).css('z-index', zIndex);
    setTimeout(function () {
      $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
    }, 0);
  });
};

app.aproveTo = function (congressId, workId) {
  var dataHtml = "Para você aprovar o trabalho, você necessita escolher para qual evento ele deverá ser movido.";
  dataHtml += '<hr><span>Congresso</span>';
  dataHtml += "<select name=\"select-congress\" id=\"select-congress\" class=\"select-region custom-select mb-3\" >";
  dataHtml += "<option selected>Selecione...</option>";

  for (var index = 0; index < app.lists.congressList.length; index++) {
    var oCongress = app.lists.congressList[index];

    if (oCongress.id != congressId) {
      dataHtml += "<option value=\"".concat(oCongress.id, "\">").concat(oCongress.name, "</option>");
    }
  }

  dataHtml += "</select>";
  Swal.fire({
    title: 'Atenção',
    html: dataHtml,
    icon: 'info',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Aprovar e Mover',
    cancelButtonText: 'Cancelar'
  }).then(function (result) {
    if (result.value) {
      var congressId = $('#select-congress').val() || ''; // Get the token from the current sessionToken, or log the user out if none is there

      var token = typeof app.user.token == 'string' ? app.user.token : false;

      if (token) {
        // Fetch the user data
        var payloadObj = {
          'host': window.location.host,
          'access_token': token,
          workId: workId,
          congressId: congressId
        };
        app.client.request(undefined, endpoint + '/api/Admins/Institution/Congress/Work/Approve', 'POST', undefined, payloadObj, function (statusCode, responsePayload) {
          if (statusCode == 200 && responsePayload) {
            Swal.fire('Uhu', 'Trabalho aprovado e movido com sucesso.', 'success');
          }
        });
      }
    }
  });
};

app.deleteWork = function (congressId, workId) {
  Swal.fire({
    title: 'Atenção',
    text: "Você realmente deseja deletar o trabalho",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Vamos lá, pode deletar o trabalho',
    cancelButtonText: 'Cancelar'
  }).then(function (result) {
    if (result.value) {
      console.log(congressId, workId); // Get the token from the current sessionToken, or log the user out if none is there

      var token = typeof app.user.token == 'string' ? app.user.token : false;

      if (token) {
        // Fetch the user data
        var queryStringObject = {
          'host': window.location.host,
          'access_token': token,
          workId: workId
        };
        console.log(queryStringObject);
        app.client.request(undefined, endpoint + '/api/Admins/Institution/Congress/Work', 'DELETE', queryStringObject, undefined, function (statusCode, responsePayload) {
          if (statusCode == 200 && responsePayload) {
            Swal.fire('Uhu', 'Registro deletado com sucesso.', 'success').then(function (e) {
              if (e.value) {
                window.location.reload();
              }
            });
          }
        });
      }
    }
  });
};

app.checkAvliatorCategoryItsSelected = function (avaliator, category) {
  for (var key in avaliator) {
    if (Object.hasOwnProperty.call(avaliator, key)) {
      var element = avaliator[key];

      if (key.indexOf(category) > -1 && element) {
        return true;
      }
    }
  }

  return false;
};

app.loadAvaliatorDetails = function (congressId, avaliatorId) {
  avaliatorId = typeof avaliatorId == 'string' && avaliatorId.length > 0 ? avaliatorId : false;
  var avaliators = _typeof(app.lists.avaliatorList) == 'object' && app.lists.avaliatorList.length > 0 ? app.lists.avaliatorList : false;

  if (avaliatorId && avaliators) {
    var _loop3 = function _loop3(index) {
      var avaliator = avaliators[index];

      if (avaliator.id == avaliatorId) {
        $('.page-hover').fadeIn(1000);
        congressId = app.congress.id;
        var dataHtml = '';
        dataHtml += '<span>IES</span>';
        dataHtml += "<select name=\"select-ies-avaliator\" id=\"select-ies-avaliator\" class=\"select-ies-avaliator custom-select mb-3\" >";
        dataHtml += "<option selected>Selecione...</option>";

        for (var key in app.lists.iesByName) {
          if (Object.hasOwnProperty.call(app.lists.iesByName, key)) {
            var ies = app.lists.iesByName[key];
            dataHtml += "<option value=\"".concat(ies.name, "\" ").concat(ies.name == avaliator.ies ? 'selected' : '', " >").concat(ies.name, "</option>");
          }
        }

        dataHtml += "</select>";
        dataHtml += '<hr><span>Categoria</span>';
        dataHtml += '<div class="d-flex-row justify-content-between mt-3">';

        for (var _index15 = 0; _index15 < app.lists.categories.length; _index15++) {
          var modalidade = app.lists.categories[_index15].split(' ');

          var category = "".concat(modalidade[0], " ").concat(modalidade[1]);
          var modality = app.lists.categories[_index15]; // console.log(app.checkAvliatorCategoryItsSelected(avaliator, category))

          dataHtml += "\n            <div class=\"form-check form-check-inline col-12\">\n              <input class=\"form-check-input checkbox-categ-avaliator\" type=\"checkbox\" id=\"".concat(modality.replace(" ", ""), "\" value=\"").concat(modality, "\" ").concat(app.checkAvliatorCategoryItsSelected(avaliator, modality) ? 'checked' : '', ">\n              <label class=\"form-check-label\" for=\"").concat(modality.replace(" ", ""), "\">").concat(modality, "</label>\n            </div>\n          ");
        }

        dataHtml += '</div>';
        Swal.fire({
          title: '<strong>Editar Avaliador</strong>',
          icon: 'info',
          html: "\n            <div class=\"col-lg-12\">\n              ".concat(dataHtml, "\n            </div>\n          "),
          showCloseButton: true,
          showCancelButton: true,
          focusConfirm: false,
          confirmButtonText: '<i class="fa fa-thumbs-up"></i> Atualizar',
          confirmButtonAriaLabel: 'Thumbs up, add!',
          cancelButtonText: '<i class="fa fa-thumbs-down"></i>',
          cancelButtonAriaLabel: 'Thumbs down'
        }).then(function (e) {
          if (e.value) {
            var categCheck = [];
            var ies = $("#select-ies-avaliator option:selected").text();
            var categories = $('.checkbox-categ-avaliator:checkbox:checked');

            for (var _index16 = 0; _index16 < categories.length; _index16++) {
              var categ = categories[_index16];
              categCheck.push(categ.value);
            }

            var token = typeof app.user.token == 'string' ? app.user.token : false;

            if (token) {
              var payloadObj = {
                'host': window.location.host,
                'access_token': token,
                'congressId': congressId,
                'categs': categCheck,
                'avaliatorId': avaliator.id,
                'ies': ies
              };
              app.client.request(undefined, endpoint + '/api/Admins/Institution/Congress/Work/Avaliator', 'POST', undefined, payloadObj, function (statusCode, responsePayload) {
                if (statusCode == 200) {
                  Swal.fire('Uhu', 'Coordenador atualizado com sucesso.', 'success').then(function (e) {
                    app.loadEventsPage(function (r) {
                      app.loadCongressDataModal('congress-detail-modal', app.congressId);
                    });
                  });
                }
              });
            }
          }
        });
      }
    };

    for (var index = 0; index < avaliators.length; index++) {
      var congressId;

      _loop3(index);
    }
  }
};

app.loadAvaliators = function (oCongress) {// Show each created check as a new row in the table
};

app.loadCongressDataModal = function (modalId, congressId) {
  for (var indiceta = 0; indiceta < app.lists.congressList.length; indiceta++) {
    var oCongress = app.lists.congressList[indiceta];

    if (oCongress.id == congressId) {
      // console.log(oCongress)
      app.congress = oCongress;
      var host = window.location.host;
      var congressDate = new Date(oCongress.date);
      congressDate.setHours(congressDate.getHours() - 3);
      var congressDateEnd = oCongress.dateEnd ? new Date(oCongress.dateEnd) : new Date();
      congressDateEnd.setHours(congressDateEnd.getHours() - 3);
      var congressDateCert = oCongress.dateCert ? new Date(oCongress.dateCert) : new Date();
      congressDateCert.setHours(congressDateCert.getHours() - 3);
      var date = congressDate.toISOString().split('.');
      var dateEnd = congressDateEnd.toISOString().split('.');
      var dateCert = congressDateCert.toISOString().split('.');
      var oAvaliatorStatus = {
        waiting: "<i class=\"fad fa-clock\"></i> Aguardando",
        ok: "<i class=\"fad fa-clipboard-list-check\"></i> Ok"
      };
      app.lists.avaliatorList = [];
      app.lists.iesByName = {};

      if (_typeof(oCongress.iesList) == 'object') {
        for (var index = 0; index < oCongress.iesList.length; index++) {
          var oIes = oCongress.iesList[index];
          app.lists.iesByName[oIes.name] = oIes;
        }
      } // Create cordinators


      if (_typeof(oCongress.cordinators) == 'object' && oCongress.cordinators) {
        // Show each created check as a new row in the table
        var dataSet = [];

        for (var _index17 = 0; _index17 < oCongress.cordinators.length; _index17++) {
          var cordinator = oCongress.cordinators[_index17];

          if (_typeof(cordinator.customer) == 'object' && cordinator.customer) {
            var customer = cordinator.customer;
            var child = [];
            child[0] = customer.name;
            child[1] = customer.email;
            child[2] = "\n              <button type=\"button\" class=\"open-modal btn btn-primary\" onclick=\"app.loadCordinatorDetail('".concat(cordinator.id, "')\">\n                <i class=\"fad fa-info-square\"></i> Detalhes\n              </button>\n              ");
            app.lists.cordinatorsList.push(cordinator);
            dataSet.push(child);
          }
        }

        if ($.fn.dataTable.isDataTable('#tableCordinators')) {
          var localTable = $('#tableCordinators').DataTable();
          localTable.destroy();
        }

        $('#tableCordinators thead th').each(function () {
          var title = $(this).text();

          if (title != "Ações") {
            $(this).html("\n                <label style=\"display: none\">".concat(title.trim(), "</label>\n                <input type=\"text\" class=\"form-control\" placeholder=\"").concat(title.trim(), "\">\n              "));
          }
        });

        if ($.fn.dataTable.isDataTable('#tableCordinators')) {
          var _localTable = $('#tableCordinators').DataTable();

          _localTable.destroy();
        }

        var tableCordinator = $('#tableCordinators').DataTable({
          scrollX: true,
          lengthChange: true,
          buttons: ['excel', 'pdf'],
          data: dataSet,
          columnDefs: [],
          order: [[1, "asc"]],
          language: {
            lengthMenu: "Exibindo _MENU_  ",
            zeroRecords: "Nenhum registro disponível.",
            info: "Exibindo pagina _PAGE_ de _PAGES_",
            search: "Filtro _INPUT_",
            paginate: {
              "next": ">",
              "previous": "<"
            }
          }
        });
        tableCordinator.columns().every(function () {
          var that = this;
          $('input', this.header()).on('keyup change', function () {
            if (that.search() !== this.value) {
              that.search(this.value).draw();
            }
          });
        });
        tableCordinator.buttons().container().appendTo('#tableCordinators_wrapper #tableCordinators_length');
        document.getElementById('nav-cordinators-tab').addEventListener('click', function () {
          setTimeout(function () {
            window.dispatchEvent(new Event('resize'));
          }, 500);
        });
      } // Create avaliators table


      if (_typeof(oCongress.avaliators) == 'object' && oCongress.avaliators) {
        var dataSet = [];
        app.lists.avaliatorList = [];

        for (var _index18 = 0; _index18 < oCongress.avaliators.length; _index18++) {
          var element = oCongress.avaliators[_index18];

          if (_typeof(element.customer) == 'object' && element.customer) {
            var _customer = element.customer;
            var _child = [];
            _child[0] = _customer.name;
            _child[1] = _customer.email;
            _child[2] = typeof element.ies == 'string' && element.ies != 'Selecione...' && element.ies.length > 0 ? element.ies : 'Não Selecionado';
            _child[3] = oAvaliatorStatus[element.status];
            _child[4] = (element.status == 'waiting' ? "<button type=\"button\" class=\"open-modal btn btn-success\" onclick=\"app.loadAvaliatorStatusChange('".concat(oCongress.id, "','").concat(element.id, "','ok')\">\n                <i class=\"fad fa-user-check\"></i>\n              </button>\n              ") : "<button type=\"button\" class=\"open-modal btn btn-danger\" onclick=\"app.loadAvaliatorStatusChange('".concat(oCongress.id, "','").concat(element.id, "','waiting')\">\n                <i class=\"fad fa-user-times\"></i>\n              </button>\n              ")) + "<button type=\"button\" class=\"btn btn-primary\" onclick=\"app.loadAvaliatorDetails('".concat(oCongress.id, "','").concat(element.id, "')\">\n              <i class=\"fad fa-info-square\"></i> Detalhes\n              </button>");
            app.lists.avaliatorList.push(element);
            dataSet.push(_child);
          }
        }

        if ($.fn.dataTable.isDataTable('#tableAvaliators')) {
          var _localTable2 = $('#tableAvaliators').DataTable();

          _localTable2.destroy();
        }

        $('#tableAvaliators thead th').each(function () {
          var title = $(this).text();

          if (title != "Ações") {
            $(this).html("\n              <label style=\"display: none\">".concat(title.trim(), "</label>\n              <input type=\"text\" class=\"form-control\" placeholder=\"").concat(title.trim(), "\">\n              "));
          }
        });

        if ($.fn.dataTable.isDataTable('#tableAvaliators')) {
          var _localTable3 = $('#tableAvaliators').DataTable();

          _localTable3.destroy();
        }

        var tble = $('#tableAvaliators').DataTable({
          scrollX: true,
          lengthChange: true,
          buttons: ['excel', 'pdf'],
          data: dataSet,
          columnDefs: [],
          order: [[1, "asc"]],
          language: {
            lengthMenu: "Exibindo _MENU_  ",
            zeroRecords: "Nenhum registro disponível.",
            info: "Exibindo pagina _PAGE_ de _PAGES_",
            search: "Filtro _INPUT_",
            paginate: {
              "next": ">",
              "previous": "<"
            }
          }
        });
        tble.columns().every(function () {
          var that = this;
          $('input', this.header()).on('keyup change', function () {
            if (that.search() !== this.value) {
              that.search(this.value).draw();
            }
          });
        });
        tble.buttons().container().appendTo('#tableAvaliators_wrapper #tableAvaliators_length');
        document.getElementById('nav-avaliators-tab').addEventListener('click', function () {
          setTimeout(function () {
            window.dispatchEvent(new Event('resize'));
          }, 500);
        });
      } // Create avaliations table


      if (_typeof(oCongress.works) == 'object' && oCongress.works) {
        // Show each created check as a new row in the table
        var dataSet = [];

        for (var _index19 = 0; _index19 < oCongress.works.length; _index19++) {
          var _element = oCongress.works[_index19];

          if (_typeof(_element.submitionWork) == 'object' && _element.submitionWork && _typeof(_element.avaliations) == 'object' && _element.avaliations) {
            var _child2 = [];
            _child2[0] = _element.workName || '';
            _child2[1] = _element.avaliations.length;
            _child2[2] = app.lists.iesByName[_element.ies] && app.lists.iesByName[_element.ies].sigla ? app.lists.iesByName[_element.ies].sigla : 'Não preenchido';
            _child2[3] = app.lists.iesByName[_element.ies] && app.lists.iesByName[_element.ies].region ? app.lists.iesByName[_element.ies].region : 'Não preenchido';
            _child2[4] = "\n              <button type=\"button\" class=\"open-modal btn btn-info\" onClick=\"app.viewAvaliation('".concat(oCongress.id, "','").concat(_element.id, "')\">\n                Visualizar Avalia\xE7\xF5es <i class=\"fad fa-clipboard-list-check\"></i>\n              </button>\n              ");
            app.lists.avaliationsList.push(_element);
            dataSet.push(_child2);
          }
        }

        if ($.fn.dataTable.isDataTable('#tableAvaliationsList')) {
          var _localTable4 = $('#tableAvaliationsList').DataTable();

          _localTable4.destroy();
        }

        $('#tableAvaliationsList thead th').each(function () {
          var title = $(this).text();

          if (title != "Ações") {
            $(this).html("\n              <label style=\"display: none\">".concat(title.trim(), "</label>\n              <input type=\"text\" class=\"form-control\" placeholder=\"").concat(title.trim(), "\">\n              "));
          }
        });
        var tble = $('#tableAvaliationsList').DataTable({
          scrollX: true,
          lengthChange: true,
          buttons: ['excel', 'pdf'],
          data: dataSet,
          columnDefs: [],
          order: [[1, "asc"]],
          language: {
            lengthMenu: "Exibindo _MENU_  ",
            zeroRecords: "Nenhum registro disponível.",
            info: "Exibindo pagina _PAGE_ de _PAGES_",
            search: "Filtro _INPUT_",
            paginate: {
              "next": ">",
              "previous": "<"
            }
          }
        });
        tble.columns().every(function () {
          var that = this;
          $('input', this.header()).on('keyup change', function () {
            if (that.search() !== this.value) {
              that.search(this.value).draw();
            }
          });
        });
        tble.buttons().container().appendTo('#tableAvaliationsList_wrapper #tableAvaliationsList_length');
        document.getElementById('nav-avaliations-tab').addEventListener('click', function () {
          setTimeout(function () {
            window.dispatchEvent(new Event('resize'));
          }, 500);
        });
      } // Create Books table


      if (_typeof(oCongress.books) == 'object' && oCongress.books) {
        // Show each created check as a new row in the table
        var dataSet = [];
        app.lists.booksByKey = {};
        app.lists.books = [];
        app.lists.indications = [];
        app.lists.workList = [];

        for (var _index20 = 0; _index20 < oCongress.books.length; _index20++) {
          var _element2 = oCongress.books[_index20];
          app.lists.booksByKey[_element2.id] = _element2;
          app.lists.books.push(_element2);
          var _child3 = [];
          _child3[0] = _element2.name || '';
          _child3[1] = _element2.area || '';
          _child3[2] = _element2.type || '';
          _child3[3] = _element2.editor || '';
          _child3[4] = oAvaliatorStatus[_element2.status];
          _child3[5] = "<button type=\"button\" class=\"open-modal btn btn-primary\" onclick=\"app.loadBookDetails('bookDetailModal','".concat(_element2.id, "')\">\n                <i class=\"fad fa-books\"></i>\n              </button>\n              ") + (_element2.status == 'waiting' ? "<button type=\"button\" class=\"open-modal btn btn-success\" onclick=\"app.loadBookStatusChange('".concat(oCongress.id, "','").concat(_element2.id, "','ok')\">\n                    <i class=\"fad fa-user-check\"></i>\n                </button>\n                ") : "<button type=\"button\" class=\"open-modal btn btn-danger\" onclick=\"app.loadBookStatusChange('".concat(oCongress.id, "','").concat(_element2.id, "','waiting')\">\n                    <i class=\"fad fa-user-times\"></i>\n                </button>\n              ")) + "\n                    <button type=\"button\" class=\"open-modal btn btn-danger\" onClick=\"app.deleteBook('".concat(oCongress.id, "','").concat(_element2.id, "')\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Excluir Livro\">\n                        <i class=\"fas fa-file-minus\"></i>\n                    </button>\n                ");
          dataSet.push(_child3);
        }

        if ($.fn.dataTable.isDataTable('#tableBooks')) {
          var _localTable5 = $('#tableBooks').DataTable();

          _localTable5.destroy();
        }

        $('#tableBooks thead th').each(function () {
          var title = $(this).text();

          if (title != "Ações") {
            $(this).html("\n                    <label style=\"display: none\">".concat(title.trim(), "</label>\n                    <input type=\"text\" class=\"form-control\" placeholder=\"").concat(title.trim(), "\">\n                "));
          }
        });
        var lasTabelas = $('#tableBooks').DataTable({
          scrollX: true,
          lengthChange: true,
          buttons: ['excel', 'pdf'],
          data: dataSet,
          columnDefs: [],
          order: [[1, "asc"]],
          language: {
            lengthMenu: "Exibindo _MENU_  ",
            zeroRecords: "Nenhum registro disponível.",
            info: "Exibindo pagina _PAGE_ de _PAGES_",
            search: "Filtro _INPUT_",
            paginate: {
              "next": ">",
              "previous": "<"
            }
          }
        });
        lasTabelas.columns().every(function () {
          var that = this;
          $('input', this.header()).on('keyup change', function () {
            if (that.search() !== this.value) {
              that.search(this.value).draw();
            }
          });
        });
        lasTabelas.buttons().container().appendTo('#tableBooks_wrapper #tableBooks_length');
        document.getElementById('nav-works-tab').addEventListener('click', function () {
          setTimeout(function () {
            window.dispatchEvent(new Event('resize'));
          }, 500);
        });
      } // Create Works table


      if (_typeof(oCongress.works) == 'object' && oCongress.works) {
        // Show each created check as a new row in the table
        var dataSet = [];
        app.lists.indications = [];
        app.lists.workList = [];

        for (var _index21 = 0; _index21 < oCongress.works.length; _index21++) {
          var _element3 = oCongress.works[_index21];
          app.lists.indications.push(_element3);
          var media = 'Não definida';
          console.log('HEYYY');

          if (_typeof(_element3.submitionWork) == 'object' && _element3.submitionWork && typeof _element3.submitionWork.workId == 'string') {
            console.log(_element3);
            var _child4 = [];

            if (typeof _element3.coerenceOfContent == 'number' && typeof _element3.experimentalismOfProduct == 'number' && typeof _element3.productQuality == 'number') {
              media = (_element3.coerenceOfContent + _element3.experimentalismOfProduct + _element3.productQuality) / 3;
            }

            _child4[0] = _element3.workName || '';
            _child4[1] = _element3.leaderName || '';
            _child4[2] = typeof _element3.ies == 'string' && app.lists.iesByName[_element3.ies] && _typeof(app.lists.iesByName[_element3.ies]) == 'object' && typeof app.lists.iesByName[_element3.ies].sigla == 'string' ? app.lists.iesByName[_element3.ies].sigla : 'Não preenchido';
            _child4[3] = typeof _element3.ies == 'string' && app.lists.iesByName[_element3.ies] && _typeof(app.lists.iesByName[_element3.ies]) == 'object' && typeof app.lists.iesByName[_element3.ies].region == 'string' ? app.lists.iesByName[_element3.ies].region : 'Não preenchido';
            _child4[4] = typeof media == 'number' ? media.toFixed(2) : media;
            _child4[5] = typeof _element3.defaultCordinatorWorkOf == 'string' ? _element3.defaultCordinatorWorkOf : "";
            _child4[6] = typeof _element3.type == 'string' ? _element3.type : "";
            _child4[7] = "\n                <button type=\"button\" class=\"open-modal btn btn-warning\" onClick=\"app.setAvaliatorToWork('".concat(oCongress.id, "','").concat(_element3.id, "')\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Atribuir Avaliador\">\n                    <i class=\"fad fa-user-plus\"></i>\n                </button>\n                <button type=\"button\" class=\"open-modal btn btn-success\" onClick=\"app.listAvaliatorsOfWork('").concat(oCongress.id, "','").concat(_element3.id, "')\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Avaliadores\">\n                    <i class=\"fas fa-list-alt\"></i>\n                </button>\n                <button type=\"button\" class=\"open-modal btn btn-danger\" onClick=\"app.deleteWork('").concat(oCongress.id, "','").concat(_element3.id, "')\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Excluir Trabalho\">\n                    <i class=\"fas fa-file-minus\"></i>\n                </button>\n                <button type=\"button\" class=\"open-modal btn btn-info\" onClick=\"app.aproveTo('").concat(oCongress.id, "','").concat(_element3.id, "')\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Aprovar e Mover trabalho\">\n                  <i class=\"fad fa-file-upload\"></i>\n                </button>\n              ");
            app.lists.workList.push(_element3);
            dataSet.push(_child4);
          }
        }

        if ($.fn.dataTable.isDataTable('#tableWorks')) {
          var _localTable6 = $('#tableWorks').DataTable();

          _localTable6.destroy();
        }

        $('#tableWorks thead th').each(function () {
          var title = $(this).text();

          if (title != "Ações") {
            $(this).html("\n                <label style=\"display: none\">".concat(title.trim(), "</label>\n                <input type=\"text\" class=\"form-control\" placeholder=\"").concat(title.trim(), "\">\n                "));
          }
        });
        var lasTabelas = $('#tableWorks').DataTable({
          scrollX: true,
          lengthChange: true,
          buttons: ['excel', 'pdf'],
          data: dataSet,
          columnDefs: [],
          order: [[1, "asc"]],
          language: {
            lengthMenu: "Exibindo _MENU_  ",
            zeroRecords: "Nenhum registro disponível.",
            info: "Exibindo pagina _PAGE_ de _PAGES_",
            search: "Filtro _INPUT_",
            paginate: {
              "next": ">",
              "previous": "<"
            }
          }
        });
        lasTabelas.columns().every(function () {
          var that = this;
          $('input', this.header()).on('keyup change', function () {
            if (that.search() !== this.value) {
              that.search(this.value).draw();
            }
          });
        });
        lasTabelas.buttons().container().appendTo('#tableWorks_wrapper #tableWorks_length');
        document.getElementById('nav-works-tab').addEventListener('click', function () {
          setTimeout(function () {
            window.dispatchEvent(new Event('resize'));
          }, 500);
        });
      }

      $('[data-toggle="tooltip"]').tooltip(); // Create Indicações table

      if (_typeof(oCongress.works) == 'object' && oCongress.works) {
        // Show each created check as a new row in the table
        var dataSet = [];

        for (var _index22 = 0; _index22 < oCongress.works.length; _index22++) {
          var _element4 = oCongress.works[_index22];
          var _child5 = [];

          var categoryAndModule = _element4.category.split(' ');

          _child5[0] = _element4.name || '';
          _child5[1] = _element4.ies || '';
          _child5[2] = "".concat(categoryAndModule[0], " ").concat(categoryAndModule[1]);
          _child5[3] = _element4.leaderName || '';
          _child5[4] = _element4.workName || '';
          _child5[5] = "\n            <button type=\"button\" class=\"open-modal btn btn-primary\" onClick=\"app.loadIndicationDetail('".concat(_element4.id, "','").concat(oCongress.id, "')\">\n              Detalhes <i class=\"fad fa-info-square\"></i>\n            </button>\n          ");
          dataSet.push(_child5);
        }

        if ($.fn.dataTable.isDataTable('#tableIndicationsWork')) {
          var _localTable7 = $('#tableIndicationsWork').DataTable();

          _localTable7.destroy();
        }

        $('#tableIndicationsWork thead th').each(function () {
          var title = $(this).text();

          if (title != "Ações") {
            $(this).html("\n              <label style=\"display: none\">".concat(title.trim(), "</label>\n              <input type=\"text\" class=\"form-control\" placeholder=\"").concat(title.trim(), "\">\n              "));
          }
        });
        var tble = $('#tableIndicationsWork').DataTable({
          scrollX: true,
          lengthChange: true,
          buttons: ['excel', 'pdf'],
          data: dataSet,
          columnDefs: [],
          order: [[1, "asc"]],
          language: {
            lengthMenu: "Exibindo _MENU_  ",
            zeroRecords: "Nenhum registro disponível.",
            info: "Exibindo pagina _PAGE_ de _PAGES_",
            search: "Filtro _INPUT_",
            paginate: {
              "next": ">",
              "previous": "<"
            }
          }
        });
        tble.columns().every(function () {
          var that = this;
          $('input', this.header()).on('keyup change', function () {
            if (that.search() !== this.value) {
              that.search(this.value).draw();
            }
          });
        });
        tble.buttons().container().appendTo('#tableIndicationsWork_wrapper #tableIndicationsWork_length');
        document.getElementById('nav-indications-tab').addEventListener('click', function () {
          setTimeout(function () {
            window.dispatchEvent(new Event('resize'));
          }, 500);
        });
      } // Create Inscrições table


      if (_typeof(oCongress.inscriptions) == 'object' && oCongress.inscriptions) {
        // Show each created check as a new row in the table
        var dataSet = [];
        var inscriptedType = {
          "inscription": "Pagante",
          "exempt": "Isento",
          "associated": "Associado"
        };
        var typeStatus = {
          "paid": "Pago",
          "chargeback": "Chargeback",
          "waiting_payment": "Aguardando"
        };

        for (var _index23 = 0; _index23 < oCongress.inscriptions.length; _index23++) {
          var oInscripted = oCongress.inscriptions[_index23];
          var oTxIns = _typeof(oInscripted.transaction) == 'object' ? oInscripted.transaction : false;

          if (_typeof(oInscripted.customer) == 'object' && typeof oInscripted.customer.name == 'string' && oInscripted.customer.name.length > 0 && typeof oInscripted.customer.email == 'string' && oInscripted.customer.email.length > 0 && typeof oInscripted.congressId == 'string' && oInscripted.congressId == oCongress.id) {
            var _child6 = [];
            _child6[0] = oInscripted.customer.name || '';
            _child6[1] = oInscripted.customer.email || '';
            _child6[2] = new Date(oInscripted.date).toLocaleString();
            _child6[3] = inscriptedType[oInscripted.type] || '';
            _child6[4] = oInscripted.oPrice.label;
            _child6[5] = oTxIns ? typeStatus[oTxIns.status] : '';
            _child6[6] = oTxIns ? +oTxIns.amount / 100 : +oInscripted.oPrice.value / 100;
            dataSet.push(_child6);
          }
        }

        if ($.fn.dataTable.isDataTable('#tableInscripteds')) {
          var _localTable8 = $('#tableInscripteds').DataTable();

          _localTable8.destroy();
        }

        $('#tableInscripteds thead th').each(function () {
          var title = $(this).text();

          if (title != "Ações") {
            $(this).html("\n              <label style=\"display: none\">".concat(title.trim(), "</label>\n              <input type=\"text\" class=\"form-control\" placeholder=\"").concat(title.trim(), "\">\n              "));
          }
        });
        var tble = $('#tableInscripteds').DataTable({
          scrollX: true,
          lengthChange: true,
          buttons: ['excel', 'pdf'],
          data: dataSet,
          columnDefs: [],
          order: [[1, "asc"]],
          language: {
            lengthMenu: "Exibindo _MENU_  ",
            zeroRecords: "Nenhum registro disponível.",
            info: "Exibindo pagina _PAGE_ de _PAGES_",
            search: "Filtro _INPUT_",
            paginate: {
              "next": ">",
              "previous": "<"
            }
          }
        });
        tble.columns().every(function () {
          var that = this;
          $('input', this.header()).on('keyup change', function () {
            if (that.search() !== this.value) {
              that.search(this.value).draw();
            }
          });
        });
        tble.buttons().container().appendTo('#tableInscripteds_wrapper #tableInscripteds_length');
        document.getElementById('nav-indications-tab').addEventListener('click', function () {
          setTimeout(function () {
            window.dispatchEvent(new Event('resize'));
          }, 500);
        });
      } // End Inscrições
      // START IES


      var max_fields_ies = 700;
      var wrapper_ies = $("#container-ies");
      $(wrapper_ies).html("\n        <div class=\"form-row mb-3\">\n          <div class=\"input-group\">\n              <div class=\"input-group-prepend\">\n                  <span class=\"input-group-text border-0\">Nome</span>\n              </div>\n              <input type=\"text\" class=\"form-control\" name=\"listies_name[0]\" id=\"iesNameZero\" placeholder=\"Nome da IES\" >\n              <div class=\"input-group-prepend\">\n                  <span class=\"input-group-text border-0\">Sigla</span>\n              </div>\n              <input type=\"text\" class=\"form-control\" name=\"listies_sigla[0]\" id=\"iesSiglaZero\" placeholder=\"Sigla da IES\" >\n              <div class=\"input-group-prepend\">\n                  <span class=\"input-group-text border-0\">Regi\xE3o</span>\n              </div>\n              <input type=\"text\" class=\"form-control\" name=\"listies_region[0]\" id=\"iesRegionZero\" placeholder=\"Rig\xE3o da IES\" >\n              <div class=\"input-group-prepend\">\n                  <span class=\"input-group-text border-0\">Estado</span>\n              </div>\n              <input type=\"text\" class=\"form-control\" name=\"listies_uf[0]\" id=\"iesStateZero\" placeholder=\"Estado da IES\" >\n              <div class=\"input-group-append\">\n                  <button class=\"btn btn-primary add_form_field_ies\" type=\"button\">\n                      <i class=\"fas fa-plus\"></i>\n                  </button>\n              </div>\n          </div>\n        </div>\n      ");

      if (_typeof(oCongress.iesList) == 'object' && oCongress.iesList instanceof Array && oCongress.iesList.length > 0) {
        $(wrapper_ies).html("");

        for (var _index24 = 0; _index24 < oCongress.iesList.length; _index24++) {
          var ies = oCongress.iesList[_index24];

          if (ies) {
            if (_index24 == 0) {
              $(wrapper_ies).html("\n                <div class=\"form-row mb-3\">\n                  <div class=\"input-group\">\n                      <div class=\"input-group-prepend\">\n                          <span class=\"input-group-text border-0\">Nome</span>\n                      </div>\n                      <input type=\"text\" class=\"form-control\" name=\"listies_name[".concat(_index24, "]\" id=\"iesNameZero\" value=\"").concat(ies.name, "\" >\n                      <div class=\"input-group-prepend\">\n                          <span class=\"input-group-text border-0\">Sigla</span>\n                      </div>\n                      <input type=\"text\" class=\"form-control\" name=\"listies_sigla[").concat(_index24, "]\" id=\"iesSiglaZero\" value=\"").concat(ies.sigla, "\" >\n                      <div class=\"input-group-prepend\">\n                          <span class=\"input-group-text border-0\">Regi\xE3o</span>\n                      </div>\n                      <input type=\"text\" class=\"form-control\" name=\"listies_region[").concat(_index24, "]\" id=\"iesRegionZero\" value=\"").concat(ies.region, "\" >\n                      <div class=\"input-group-prepend\">\n                          <span class=\"input-group-text border-0\">Estado</span>\n                      </div>\n                      <input type=\"text\" class=\"form-control\" name=\"listies_uf[").concat(_index24, "]\" id=\"iesStateZero\" value=\"").concat(ies.uf, "\" >\n                      <div class=\"input-group-append\">\n                          <button class=\"btn btn-primary add_form_field_ies\" type=\"button\">\n                              <i class=\"fas fa-plus\"></i>\n                          </button>\n                      </div>\n                  </div>\n                </div>\n              "));
            } else {
              window.counterIesList++;
              $(wrapper_ies).append("\n                <div class=\"form-row mb-3\">\n                  <div class=\"input-group\">\n                    <div class=\"input-group-prepend\">\n                        <span class=\"input-group-text border-0\">Nome</span>\n                    </div>\n                    <input type=\"text\" class=\"form-control\" name=\"listies_name[".concat(_index24, "]\"  value=\"").concat(ies.name, "\" >\n                    <div class=\"input-group-prepend\">\n                        <span class=\"input-group-text border-0\">Sigla</span>\n                    </div>\n                    <input type=\"text\" class=\"form-control\" name=\"listies_sigla[").concat(_index24, "]\" value=\"").concat(ies.sigla, "\" >\n                    <div class=\"input-group-prepend\">\n                        <span class=\"input-group-text border-0\">Regi\xE3o</span>\n                    </div>\n                    <input type=\"text\" class=\"form-control\" name=\"listies_region[").concat(_index24, "]\" value=\"").concat(ies.region, "\" >\n                    <div class=\"input-group-prepend\">\n                        <span class=\"input-group-text border-0\">Estado</span>\n                    </div>\n                    <input type=\"text\" class=\"form-control\" name=\"listies_uf[").concat(_index24, "]\" value=\"").concat(ies.uf, "\" >\n                    <div class=\"input-group-append\">\n                      <button class=\"btn btn-danger delete_ies\">\n                        <i class=\"fas fa-minus\"></i>\n                      </button>\n                    </div>\n                  </div>\n                </div>\n              "));
            }
          }
        }

        $('.las-subdivs').fadeIn();
      }

      $('body').on('click', 'button.add_form_field_ies', function (e) {
        e.preventDefault();

        if (window.counterIesList < max_fields_ies) {
          window.counterIesList++;
          $(wrapper_ies).append("\n            <div class=\"form-row mb-3\">\n              <div class=\"input-group\">\n              <div class=\"input-group-prepend\">\n                  <span class=\"input-group-text border-0\">Nome</span>\n              </div>\n              <input type=\"text\" class=\"form-control\" name=\"listies_name[".concat(counterIesList, "]\"   >\n              <div class=\"input-group-prepend\">\n                  <span class=\"input-group-text border-0\">Sigla</span>\n              </div>\n              <input type=\"text\" class=\"form-control\" name=\"listies_sigla[").concat(counterIesList, "]\"  >\n              <div class=\"input-group-prepend\">\n                  <span class=\"input-group-text border-0\">Regi\xE3o</span>\n              </div>\n              <input type=\"text\" class=\"form-control\" name=\"listies_region[").concat(counterIesList, "]\"  >\n              <div class=\"input-group-prepend\">\n                  <span class=\"input-group-text border-0\">Estado</span>\n              </div>\n              <input type=\"text\" class=\"form-control\" name=\"listies_uf[").concat(counterIesList, "]\"  > \n                <div class=\"input-group-append\">\n                  <button class=\"btn btn-danger delete_ies\">\n                    <i class=\"fas fa-minus\"></i>\n                  </button>\n                </div>\n              </div>\n            </div>\n          "));
        } else {
          Swal.fire('Ops', 'Você não pode adicionar mais campos', 'error');
        }
      });
      $(wrapper_ies).on("click", ".delete_ies", function (e) {
        e.preventDefault();
        $(this).parent('div').parent('div').parent('div').remove();
        window.counterIesList--;
      }); // END IES
      // START CATEGS

      var max_fields_categs = 100;
      var wrapper_categs = $("#container-categs");
      app.lists.categList = [];
      app.lists.categories = [];

      if (_typeof(oCongress.categories) == 'object' && oCongress.categories instanceof Array && oCongress.categories.length > 0) {
        $(wrapper_categs).html("");

        for (var _index25 = 0; _index25 < oCongress.categories.length; _index25++) {
          var categ = oCongress.categories[_index25];

          if (categ) {
            var aCateg = categ.split(" ");
            app.lists.categList.push("".concat(categ));
            app.lists.categories.push(categ);

            if (_index25 == 0) {
              $(wrapper_categs).html("\n                <div class=\"form-row mb-3\">\n                  <div class=\"input-group\">\n                      <div class=\"input-group-prepend\">\n                          <span class=\"input-group-text border-0\">Nome</span>\n                      </div>\n                      <input type=\"text\" class=\"form-control\" name=\"categ[".concat(_index25, "]\" id=\"categZero\" value=\"").concat(categ, "\" required>\n                      <div class=\"input-group-append\">\n                          <button class=\"btn btn-primary add_form_field_categ\" type=\"button\">\n                              <i class=\"fas fa-plus\"></i>\n                          </button>\n                      </div>\n                  </div>\n                </div>\n              "));
            } else {
              window.counterCategs++;
              $(wrapper_categs).append("\n                <div class=\"form-row mb-3\">\n                  <div class=\"input-group\">\n                    <div class=\"input-group-prepend\">\n                      <span class=\"input-group-text border-0\">Nome</span>\n                    </div>\n                    <input type=\"text\" class=\"form-control\" name=\"categ[".concat(_index25, "]\" value=\"").concat(categ, "\" required>\n                    <div class=\"input-group-append\">\n                      <button class=\"btn btn-danger delete_categ\">\n                        <i class=\"fas fa-minus\"></i>\n                      </button>\n                    </div>\n                  </div>\n                </div>\n              "));
            }
          }
        }

        $('.las-subdivs').fadeIn();
      } else {
        $(wrapper_categs).html("\n          <div class=\"form-row mb-3\">\n            <div class=\"input-group\">\n                <div class=\"input-group-prepend\">\n                    <span class=\"input-group-text border-0\">Ref</span>\n                </div>\n                <input type=\"text\" class=\"form-control\" name=\"categ[0]\" id=\"categZero\" placeholder=\"Category\">\n                <div class=\"input-group-append\">\n                    <button class=\"btn btn-primary add_form_field_categ\" type=\"button\">\n                        <i class=\"fas fa-plus\"></i>\n                    </button>\n                </div>\n            </div>\n          </div>\n        ");
      }

      $('body').on('click', 'button.add_form_field_categ', function (e) {
        e.preventDefault();

        if (window.counterCategs < max_fields_categs) {
          window.counterCategs++;
          $(wrapper_categs).append("\n            <div class=\"form-row mb-3\">\n              <div class=\"input-group\">\n                <div class=\"input-group-prepend\">\n                  <span class=\"input-group-text border-0\">Ref</span>\n                </div>\n                <input type=\"text\" class=\"form-control\" name=\"categ[".concat(window.counterCategs, "]\" required>\n                <div class=\"input-group-append\">\n                  <button class=\"btn btn-danger delete_categ\">\n                    <i class=\"fas fa-minus\"></i>\n                  </button>\n                </div>\n              </div>\n            </div>\n          "));
        } else {
          Swal.fire('Ops', 'Você não pode adicionar mais campos', 'error');
        }
      });
      $(wrapper_categs).on("click", ".delete_categ", function (e) {
        e.preventDefault();
        $(this).parent('div').parent('div').parent('div').remove();
        window.counterCategs--;
      }); // END CATEGS
      // START SUBDIV

      var max_fields_subdivs = 11;
      var wrapper_container = $("#container-subdivs");
      $("#subdivision").change(function () {
        var checked = $(this).is(":checked");

        if (checked) {
          $('.las-subdivs').fadeIn();
          $(wrapper_container).html("\n            <div class=\"form-row mb-3\">\n              <div class=\"input-group\">\n                  <div class=\"input-group-prepend\">\n                      <span class=\"input-group-text border-0\">Nome</span>\n                  </div>\n                  <input type=\"text\" class=\"form-control\" name=\"sbdiv[0]\" id=\"sbdivZero\" placeholder=\"Nordeste\" required>\n                  <div class=\"input-group-append\">\n                      <button class=\"btn btn-primary add_form_field_subdiv\" type=\"button\">\n                          <i class=\"fas fa-plus\"></i>\n                      </button>\n                  </div>\n              </div>\n            </div>\n          ");
        } else {
          $('.las-subdivs').fadeOut('fast');
          $(wrapper_container).html("");
        }
      });
      document.getElementById('subdivision').checked = typeof oCongress.subdivision == 'boolean' ? oCongress.subdivision : false;
      document.getElementById('subdivision').disabled = typeof oCongress.subdivision == 'boolean' ? oCongress.subdivision : false;

      if (_typeof(oCongress.subdivs) == 'object' && oCongress.subdivs instanceof Array && oCongress.subdivs.length > 0) {
        $(wrapper_container).html("");

        for (var _index26 = 0; _index26 < oCongress.subdivs.length; _index26++) {
          var subdiv = oCongress.subdivs[_index26];

          if (subdiv) {
            if (_index26 == 0) {
              $(wrapper_container).html("\n                <div class=\"form-row mb-3\">\n                  <div class=\"input-group\">\n                      <div class=\"input-group-prepend\">\n                          <span class=\"input-group-text border-0\">Nome</span>\n                      </div>\n                      <input type=\"text\" class=\"form-control\" name=\"sbdiv[".concat(_index26, "]\" id=\"sbdivZero\" value=\"").concat(subdiv, "\" required>\n                      <div class=\"input-group-append\">\n                          <button class=\"btn btn-primary add_form_field_subdiv\" type=\"button\">\n                              <i class=\"fas fa-plus\"></i>\n                          </button>\n                      </div>\n                  </div>\n                </div>\n              "));
            } else {
              window.counterSubdivs++;
              $(wrapper_container).append("\n                <div class=\"form-row mb-3\">\n                  <div class=\"input-group\">\n                    <div class=\"input-group-prepend\">\n                      <span class=\"input-group-text border-0\">Nome</span>\n                    </div>\n                    <input type=\"text\" class=\"form-control\" name=\"sbdiv[".concat(_index26, "]\" value=\"").concat(subdiv, "\" required>\n                    <div class=\"input-group-append\">\n                      <button class=\"btn btn-danger delete_subdiv_min\">\n                        <i class=\"fas fa-minus\"></i>\n                      </button>\n                    </div>\n                  </div>\n                </div>\n              "));
            }
          }
        }

        $('.las-subdivs').fadeIn();
      }

      $('body').on('click', 'button.add_form_field_subdiv', function (e) {
        e.preventDefault();

        if (window.counterSubdivs < max_fields_subdivs) {
          window.counterSubdivs++;
          $(wrapper_container).append("\n            <div class=\"form-row mb-3\">\n              <div class=\"input-group\">\n                <div class=\"input-group-prepend\">\n                  <span class=\"input-group-text border-0\">Nome</span>\n                </div>\n                <input type=\"text\" class=\"form-control\" name=\"sbdiv[".concat(window.counterSubdivs, "]\" required>\n                <div class=\"input-group-append\">\n                  <button class=\"btn btn-danger delete_subdiv_min\">\n                    <i class=\"fas fa-minus\"></i>\n                  </button>\n                </div>\n              </div>\n            </div>\n          "));
        } else {
          Swal.fire('Ops', 'Você não pode adicionar mais campos', 'error');
        }
      });
      $(wrapper_container).on("click", ".delete_subdiv_min", function (e) {
        e.preventDefault();
        $(this).parent('div').parent('div').parent('div').remove();
        window.counterSubdivs--;
      }); // END SUBDIVS
      // STAR PRODUCT PRICE

      var max_fields = 11;
      var wrapper = $("#container-choices");
      $(wrapper).html("\n        <div class=\"form-row mb-3\">\n          <div class=\"input-group\">\n              <div class=\"input-group-prepend\">\n                  <span class=\"input-group-text border-0\">Descri\xE7\xE3o</span>\n              </div>\n              <input type=\"text\" class=\"form-control\" name=\"price_text[0]\" id=\"priceZeroText\" placeholder=\"Estudantes\" required>\n              <div class=\"input-group-prepend\">\n                <span class=\"input-group-text border-0\">R$</span>\n              </div>\n              <input type=\"number\" class=\"form-control\" name=\"price_value[0]\" id=\"priceZeroValue\" placeholder=\"100,00\" required>\n              <div class=\"input-group-append\">\n                  <button class=\"btn btn-primary add_form_field\" type=\"button\">\n                      <i class=\"fas fa-plus\"></i>\n                  </button>\n              </div>\n          </div>\n        </div>\n      ");

      if (_typeof(oCongress.products) == 'object' && oCongress.products instanceof Array && oCongress.products.length > 0) {
        $(wrapper).html("");

        for (var _index27 = 0; _index27 < oCongress.products.length; _index27++) {
          var product = oCongress.products[_index27];

          if (product) {
            if (_index27 == 0) {
              $(wrapper).append("\n                <div class=\"form-row mb-3\">\n                  <div class=\"input-group\">\n                      <div class=\"input-group-prepend\">\n                          <span class=\"input-group-text border-0\">Descri\xE7\xE3o</span>\n                      </div>\n                      <input type=\"text\" class=\"form-control\" name=\"price_text[0]\" value=\"".concat(product.text, "\" id=\"priceZeroText\" placeholder=\"Estudantes\" required>\n                      <div class=\"input-group-prepend\">\n                        <span class=\"input-group-text border-0\">R$</span>\n                      </div>\n                      <input type=\"number\" class=\"form-control\" name=\"price_value[0]\" value=\"").concat(product.value / 100, "\" id=\"priceZeroValue\" placeholder=\"100,00\" required>\n                      <div class=\"input-group-append\">\n                          <button class=\"btn btn-primary add_form_field\" type=\"button\">\n                              <i class=\"fas fa-plus\"></i>\n                          </button>\n                      </div>\n                  </div>\n                </div>\n              "));
            } else {
              window.counterPriceValue++;
              $(wrapper).append("\n                <div class=\"form-row mb-3\">\n                  <div class=\"input-group\">\n                    <div class=\"input-group-prepend\">\n                      <span class=\"input-group-text border-0\">Descri\xE7\xE3o</span>\n                    </div>\n                    <input type=\"text\" class=\"form-control\" name=\"price_text[".concat(window.counterPriceValue, "]\" value=\"").concat(product.text, "\" required>\n                    <div class=\"input-group-prepend\">\n                      <span class=\"input-group-text border-0\">R$</span>\n                    </div>\n                    <input type=\"number\" class=\"form-control\" name=\"price_value[").concat(window.counterPriceValue, "]\" value=\"").concat(product.value / 100, "\" required>\n                    <div class=\"input-group-append\">\n                      <button class=\"btn btn-danger delete\">\n                        <i class=\"fas fa-minus\"></i>\n                      </button>\n                    </div>\n                  </div>\n                </div>\n              "));
            }
          }
        }
      }

      $('body').on('click', 'button.add_form_field', function (e) {
        e.preventDefault();

        if (window.counterPriceValue < max_fields) {
          window.counterPriceValue++;
          $(wrapper).append("\n            <div class=\"form-row mb-3\">\n              <div class=\"input-group\">\n                <div class=\"input-group-prepend\">\n                  <span class=\"input-group-text border-0\">Descri\xE7\xE3o</span>\n                </div>\n                <input type=\"text\" class=\"form-control\" name=\"price_text[".concat(window.counterPriceValue, "]\" required>\n                <div class=\"input-group-prepend\">\n                  <span class=\"input-group-text border-0\">R$</span>\n                </div>\n                <input type=\"number\" class=\"form-control\" name=\"price_value[").concat(window.counterPriceValue, "]\" required>\n                <div class=\"input-group-append\">\n                  <button class=\"btn btn-danger delete\">\n                    <i class=\"fas fa-minus\"></i>\n                  </button>\n                </div>\n              </div>\n            </div>\n          "));
        } else {
          Swal.fire('Ops', 'Você não pode adicionar mais campos', 'error');
        }
      });
      $(wrapper).on("click", ".delete", function (e) {
        e.preventDefault();
        $(this).parent('div').parent('div').parent('div').remove();
        window.counterPriceValue--;
      }); //  END PRODUCTS PRICE

      if (document.querySelector('.ck.ck-reset.ck-editor.ck-rounded-corners')) document.querySelector('.ck.ck-reset.ck-editor.ck-rounded-corners').remove();

      if (typeof oCongress.topImageUrl == 'string' && oCongress.topImageUrl.length > 0) {
        document.getElementById('topImageUrl').value = oCongress.topImageUrl;
        document.getElementById('topImageDisplay').src = oCongress.topImageUrl;
      }

      if (typeof oCongress.certImageUrl == 'string' && oCongress.certImageUrl.length > 0) {
        document.getElementById('certImageUrl').value = oCongress.certImageUrl;
        document.getElementById('certImageDisplay').src = oCongress.certImageUrl;
      } else {
        document.getElementById('certImageUrl').value = "";
        document.getElementById('certImageDisplay').src = "https://img.icons8.com/carbon-copy/2x/image.png";
      }

      document.getElementById('congressmanCert').innerText = typeof oCongress.congressmanCert == 'string' && oCongress.congressmanCert.length > 0 ? oCongress.congressmanCert : "";
      document.getElementById('avaliatorCert').innerText = typeof oCongress.avaliatorCert == 'string' && oCongress.avaliatorCert.length > 0 ? oCongress.avaliatorCert : "";
      document.getElementById('coordinatorCert').innerText = typeof oCongress.coordinatorCert == 'string' && oCongress.coordinatorCert.length > 0 ? oCongress.coordinatorCert : "";
      document.getElementById('acceptedWorkCert').innerText = typeof oCongress.acceptedWorkCert == 'string' && oCongress.acceptedWorkCert.length > 0 ? oCongress.acceptedWorkCert : "";
      document.getElementById('dateCert').value = dateCert[0];
      document.getElementById('resumeCongress').innerText = typeof oCongress.resume == 'string' && oCongress.resume.length > 0 ? oCongress.resume : "Descreva o seu evento.";
      ClassicEditor.create(document.querySelector('#resumeCongress'), {
        cloudServices: {
          tokenUrl: 'https://79675.cke-cs.com/token/dev/05f801120cf7cccdb540ea25a7068d7d1c8108b02232eb1bfec6b89be13c',
          uploadUrl: 'https://79675.cke-cs.com/easyimage/upload/'
        }
      }).then(function (editor) {
        app.ckeditor.resumeCongress = editor;
      })["catch"](function (error) {
        console.error(error);
      });
      document.getElementById('status').value = typeof oCongress.status == 'string' && oCongress.status.length > 0 ? oCongress.status : "";
      document.getElementById('pix').checked = _typeof(oCongress.payment_methods) == 'object' && typeof oCongress.payment_methods.pix == 'boolean' ? oCongress.payment_methods.pix : false;
      document.getElementById('boleto').checked = _typeof(oCongress.payment_methods) == 'object' && typeof oCongress.payment_methods.boleto == 'boolean' ? oCongress.payment_methods.boleto : false;
      document.getElementById('credit_card').checked = _typeof(oCongress.payment_methods) == 'object' && typeof oCongress.payment_methods.credit_card == 'boolean' ? oCongress.payment_methods.credit_card : false;
      document.getElementById('partner-free').checked = typeof oCongress.partnerFree == 'boolean' ? oCongress.partnerFree : false;
      document.querySelector("#updateEvent #eventHost").value = host;
      document.getElementById('congressId').value = oCongress.id;
      document.getElementById('nameEdit').value = oCongress.name;
      console.log(oCongress);
      document.getElementById('cutNote').value = oCongress.cutNote || 0;
      document.getElementById('worksByCateg').value = oCongress.worksByCateg || 0;
      document.getElementById('dateEdit').value = date[0];
      document.getElementById('dateEndEdit').value = dateEnd[0];
      document.getElementById('urlEdit').value = oCongress.url;
      document.getElementById('type').value = oCongress.type || "";
      $('#' + modalId).modal('show');
    }
  }
};

app["delete"] = function (referenceId, referenceModel) {
  // Get the token from the current sessionToken, or log the user out if none is there
  var token = typeof app.user.token == 'string' ? app.user.token : false;

  if (token) {
    // Fetch the user data
    var queryStringObject = {
      'host': window.location.host,
      'access_token': token,
      referenceId: referenceId,
      referenceModel: referenceModel
    };
    app.client.request(undefined, endpoint + '/api/Admins/Institution', 'DELETE', queryStringObject, undefined, function (statusCode, responsePayload) {
      if (statusCode == 200 && responsePayload) {
        Swal.fire('Uhu', 'Registro deletado com sucesso.', 'success').then(function (e) {
          if (e.value) {
            window.location.reload();
          }
        });
      }
    });
  }
};

app.editProduct = function (productId) {
  var oProduct = app.products.filter(function (p) {
    return p.id == productId;
  })[0];
  var productDate = new Date(oProduct.date);
  productDate.setDate(productDate.getDate() + 1);
  var productDateEnd = new Date(oProduct.dateend);
  productDateEnd.setDate(productDateEnd.getDate() + 1);
  document.getElementById('productIdEdit').value = productId;
  document.getElementById('nameEdit').value = oProduct.name;
  document.getElementById('descriptionEdit').value = oProduct.description;
  document.getElementById('priceEdit').value = oProduct.price / 100;
  document.getElementById('dateEdit').value = "".concat(productDate.getFullYear().toString(), "-").concat((productDate.getMonth() + 1).toString().padStart(2, 0), "-").concat(productDate.getDate().toString().padStart(2, 0));
  document.getElementById('dateendEdit').value = "".concat(productDateEnd.getFullYear().toString(), "-").concat((productDateEnd.getMonth() + 1).toString().padStart(2, 0), "-").concat(productDateEnd.getDate().toString().padStart(2, 0));
  document.getElementById('durationEdit').value = oProduct.duration;
  document.getElementById('defaultEdit').checked = oProduct["default"];
  document.getElementById('amnestyEdit').checked = oProduct.amnesty;
  $('#modalProductEdit').modal('show');
}; // Load staff creat upload file


app.uploadFile = function (fieldInputId, fieldOutputId, imageShow) {
  // Get the token from the current sessionToken, or log the user out if none is there
  var token = typeof app.user.token == 'string' ? app.user.token : false;

  if (token) {
    document.getElementById(fieldInputId).addEventListener('change', function (data) {
      $(".page-hover").html('<span id="upload-text">Subindo arquivo</span><div class="loader-cards"></div>');
      var uploadImageURL = "".concat(endpoint, "/api/Admins/Upload?access_token=").concat(token);
      var file = document.getElementById(fieldInputId).files[0];

      if (_typeof(file) == 'object') {
        if (file.size < 1000000) {
          $('.page-hover').fadeIn(1000);
          var form = new FormData();
          form.append("file", file);
          var settings = {
            "async": true,
            "crossDomain": true,
            "url": uploadImageURL,
            "method": "POST",
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data",
            "data": form
          };
          $.ajax(settings).done(function (response) {
            var inputEl = document.getElementById(fieldOutputId);
            var elm = document.getElementById(imageShow);

            if (JSON.parse(response).Location && elm && inputEl) {
              elm.src = JSON.parse(response).Location;
              inputEl.value = JSON.parse(response).Location;
            }

            $('.page-hover').fadeOut(1000);
          });
        } else {
          Swal.fire('Ops', 'O seu arquivo precisa ser menor que 1 mb', 'error');
        }
      }
    });
  }
};

app.loadIsentation = function (uid) {
  for (var index = 0; index < app.lists.user.length; index++) {
    var user = app.lists.user[index];

    if (uid == user.id) {
      (function () {
        var listObject = {};
        var dataHtml = '';
        dataHtml += "<select name=\"isentationProduct\" id=\"isentationProduct\" class=\"custom-select mb-3\" required>";
        dataHtml += "<option selected>Selecione...</option>";

        for (var _index28 = 0; _index28 < app.lists.product.length; _index28++) {
          var product = app.lists.product[_index28];
          dataHtml += "<option value=\"".concat(product.id, "\">").concat(product.name, "</option>");
          listObject[product.id] = product;
        }

        dataHtml += "</select>";
        Swal.fire({
          title: '<strong>Isenções</strong>',
          icon: 'warning',
          html: "\n          <div class=\"col-lg-12\">\n            <label for=\"isentationProduct\">Selecione o que deseja isentar</label>\n            ".concat(dataHtml, "\n          </div>\n        "),
          showCloseButton: true,
          showCancelButton: true,
          focusConfirm: false,
          confirmButtonText: '<i class="fa fa-thumbs-up"></i> Isentar',
          confirmButtonAriaLabel: 'Thumbs up, add!',
          cancelButtonText: '<i class="fa fa-thumbs-down"></i>',
          cancelButtonAriaLabel: 'Thumbs down'
        }).then(function (e) {
          if (e.value) {
            var optionId = $("#isentationProduct option:selected").val();
            var payloadObj = {
              'access_token': app.user.token,
              host: window.location.host,
              productId: optionId,
              customerId: uid,
              productTypeIsentation: listObject[optionId]['productTypeIsentation']
            };
            app.client.request(undefined, endpoint + '/api/Admins/Institution/Customer/Access', 'POST', undefined, payloadObj, function (statusCode, responsePayload) {
              if (statusCode == 200 && responsePayload) {
                if (typeof responsePayload.link == 'string' && responsePayload.link == listObject[optionId]['productTypeIsentation'] || typeof responsePayload.type == 'string' && responsePayload.type == listObject[optionId]['productTypeIsentation']) {
                  Swal.fire({
                    title: '<strong>Isenções</strong>',
                    icon: 'success',
                    html: "Parab\xE9ns isen\xE7\xE3o realizada com sucesso",
                    showCloseButton: true,
                    showCancelButton: false,
                    focusConfirm: false,
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Ok',
                    confirmButtonAriaLabel: 'Thumbs up, add!'
                  }).then(function (e) {
                    window.location.reload();
                  });
                } else {
                  Swal.fire({
                    title: '<strong>Isenções</strong>',
                    icon: 'error',
                    html: "Infelizmente a isen\xE7\xE3o n\xE3o pode ser realizada, caso esse problema aconte\xE7a com frequ\xEAncia informe o suporte.",
                    showCloseButton: true,
                    showCancelButton: false,
                    focusConfirm: false,
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Ok',
                    confirmButtonAriaLabel: 'Thumbs up, add!'
                  });
                }
              }
            });
          }
        });
      })();
    }
  }
};

app.getCongresses = function (queryStringObject) {
  app.client.request(undefined, endpoint + '/api/Admins/Institution/Congress', 'GET', queryStringObject, undefined, function (statusCode, responsePayload) {
    if (statusCode == 200 && responsePayload) {
      for (var index = 0; index < responsePayload.length; index++) {
        var oProduct = responsePayload[index];
        oProduct['productTypeIsentation'] = 'inscription';
        app.lists.product.push(oProduct);
      }
    }
  });
};

app.getProducts = function (queryStringObject) {
  app.client.request(undefined, endpoint + '/api/Admins/Institution/Products', 'GET', queryStringObject, undefined, function (statusCode, responsePayload) {
    if (statusCode == 200 && responsePayload) {
      for (var index = 0; index < responsePayload.length; index++) {
        var oProduct = responsePayload[index];

        if (oProduct["default"]) {
          oProduct['productTypeIsentation'] = 'membership';
          app.lists.product.push(oProduct);
        }
      }
    }
  });
};
/**
 * 
 * Set to variable products the list of all avaiable products
 * @param {object} queryStringObject Query string with data to request admin host and access_token
 */


app.listProducts = function (queryStringObject) {
  app.lists.product = [];
  app.getCongresses(queryStringObject);
  app.getProducts(queryStringObject);
};

app.loadEventsPage = function (callback) {
  var oStatus = {
    waiting: "<i class=\"fad fa-clock\"></i> Aguardando",
    published: "<i class=\"fad fa-clipboard-list-check\"></i> Publicado",
    archived: "<i class=\"fad fa-archive\"></i> Arquivado",
    premise: "<i class=\"fad fa-clouds\"></i> Dispon\xEDvel em Cloud"
  }; // Get the token from the current sessionToken, or log the user out if none is there

  var token = typeof app.user.token == 'string' ? app.user.token : false;

  if (token) {
    // Fetch the user data
    var queryStringObject = {
      'host': window.location.host,
      'access_token': token
    };
    app.client.request(undefined, endpoint + '/api/Admins/Institution/Users', 'GET', queryStringObject, undefined, function (statusCode, responsePayload) {
      if (statusCode == 200 && responsePayload) {
        app.lists.userList = [];

        for (var index = 0; index < responsePayload.length; index++) {
          var element = responsePayload[index];
          app.lists.userList.push(element);
        }
      }
    }); // Fetch the user data

    var queryStringObject = {
      'host': window.location.host,
      'access_token': token
    };
    app.lists.congressList = [];
    app.lists.workList = [];
    app.lists.avaliatorList = [];
    app.lists.cordinatorsList = [];
    app.lists.avaliationsList = []; // app.lists.categList = [];

    app.client.request(undefined, endpoint + '/api/Admins/Institution/Congress', 'GET', queryStringObject, undefined, function (statusCode, responsePayload) {
      if (statusCode == 200 && responsePayload) {
        app.bindCordinatorButton();
        window.counterPriceValue = 0;
        window.counterSubdivs = 0;
        window.counterCategs = 0;
        window.counterIesList = 0; // Show each created check as a new row in the table

        var dataSet = [];

        for (var index = 0; index < responsePayload.length; index++) {
          var element = responsePayload[index];
          var child = [];
          child[0] = element.name;
          child[1] = new Date(element.date).toLocaleString();
          child[2] = element.invitedTotal || '--';
          child[3] = oStatus[element.status] || '<i class="fad fa-clock"></i> Aguardando';
          child[4] = element.balance || '--';
          child[5] = "\n            <button class=\"open-modal btn btn-primary\" onClick=\"app.loadCongressDataModal('congress-detail-modal','".concat(element.id, "')\">\n              Detalhes <i class=\"fad fa-info-circle\"></i>\n            </button>\n            &nbsp;\n            <a target=\"_blank\" href=\"//").concat(element.client.front, "/indication.html?node=").concat(element.id, "\" class=\"btn btn-outline-warning\">\n              Indica\xE7\xE3o <i class=\"fad fa-user-check\"></i>\n            </a>\n            \n            ");
          app.lists.congressList.push(element);
          dataSet.push(child);
        }

        if ($.fn.dataTable.isDataTable('#tableUsers')) {
          var localTable = $('#tableUsers').DataTable();
          localTable.destroy();
        }

        $('#tableUsers thead th').each(function () {
          var title = $(this).text();

          if (title != "Ações") {
            $(this).html("\n              <label style=\"display: none\">".concat(title.trim(), "</label>\n              <input type=\"text\" class=\"form-control\" placeholder=\"").concat(title.trim(), "\">\n              "));
          }
        });
        var table = $('#tableUsers').DataTable({
          scrollX: true,
          lengthChange: true,
          buttons: ['excel', 'pdf'],
          data: dataSet,
          columnDefs: [],
          order: [[1, "asc"]],
          language: {
            lengthMenu: "Exibindo _MENU_  ",
            zeroRecords: "Nenhum registro disponível.",
            info: "Exibindo pagina _PAGE_ de _PAGES_",
            search: "Filtro _INPUT_",
            paginate: {
              "next": ">",
              "previous": "<"
            }
          }
        });
        table.columns().every(function () {
          var that = this;
          $('input', this.header()).on('keyup change', function () {
            if (that.search() !== this.value) {
              that.search(this.value).draw();
            }
          });
        });
        setTimeout(function () {
          window.dispatchEvent(new Event('resize'));
        }, 1000);
        $('.page-hover').fadeOut();
        callback(responsePayload);
      }
    });
  } else {
    app.logUserOut();
  }
};

app.loadByClass = function () {
  $('[data-toggle="offcanvas"]').on('click', function () {
    $('.offcanvas-collapse').toggleClass('open');
  }); // Get the current page from the body class

  var bodyClasses = document.querySelector("body").classList;
  var primaryClass = typeof bodyClasses[0] == 'string' ? bodyClasses[0] : false;

  if (primaryClass == "accountDashboard") {
    // register globally
    Chart.plugins.register(window.ChartDataLabels);
    setTimeout(function () {
      var token = typeof app.user.token == 'string' ? app.user.token : false;

      if (token) {
        // Fetch the user data
        var queryStringObject = {
          'host': window.location.host,
          'access_token': token
        };
        app.client.request(undefined, endpoint + '/api/Admins/Institution/Users', 'GET', queryStringObject, undefined, function (statusCode, responsePayload) {
          if (statusCode == 200 && responsePayload) {
            $('.page-hover').fadeOut();
            var oHandleAssociated = app.handleAssociated(responsePayload);

            if (oHandleAssociated) {
              var horizontalBarChartData = {
                labels: ['Usuários na base'],
                datasets: [{
                  label: 'Ativos',
                  backgroundColor: 'rgba(54, 162, 235, 0.2)',
                  borderColor: 'rgba(54, 162, 235, 1)',
                  borderWidth: 1,
                  data: [oHandleAssociated.paid]
                }, {
                  label: 'Inativos',
                  backgroundColor: 'rgba(255, 99, 132, 0.2)',
                  borderColor: 'rgba(255, 99, 132, 1)',
                  borderWidth: 1,
                  data: [oHandleAssociated.unpaid]
                }]
              };
              var ctx0 = document.getElementById('zeroChart').getContext('2d');
              window.myHorizontalBar = new Chart(ctx0, {
                type: 'bar',
                data: horizontalBarChartData,
                options: {
                  title: {
                    display: true,
                    text: 'Associados'
                  },
                  scales: {
                    yAxes: [{
                      ticks: {
                        beginAtZero: true
                      }
                    }]
                  }
                }
              });
            }

            var aTransaction = app.getPayByMont(responsePayload);

            if (aTransaction) {
              var ctx2 = document.getElementById('secChart').getContext('2d');
              var myChart2 = new Chart(ctx2, {
                type: 'line',
                data: {
                  labels: ["Janeiro", "Fevereiro", "Março", "Abril", "Mail", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
                  datasets: [{
                    label: 'Valor (R$)',
                    data: aTransaction,
                    backgroundColor: 'rgba(255, 206, 86, 0.2)',
                    borderColor: 'rgba(255, 206, 86, 1)',
                    borderWidth: 1
                  }]
                },
                options: {
                  title: {
                    display: true,
                    text: 'Pagamentos'
                  },
                  scales: {
                    yAxes: [{
                      ticks: {
                        beginAtZero: true
                      }
                    }]
                  }
                }
              });
            }

            var aGenderUser = app.groupBy(responsePayload, 'gender');

            if (aGenderUser) {
              var dataset = [];

              for (var key in aGenderUser) {
                if (Object.hasOwnProperty.call(aGenderUser, key)) {
                  var element = aGenderUser[key];
                  dataset.push(element.length);
                }
              }

              var ctx1 = document.getElementById('firstChart').getContext('2d');
              var myChart1 = new Chart(ctx1, {
                type: 'pie',
                data: {
                  labels: Object.keys(aGenderUser),
                  datasets: [{
                    label: 'Quantidade',
                    data: dataset,
                    backgroundColor: ['rgba(54, 162, 235, 0.2)', 'rgba(255, 99, 132, 0.2)', 'rgba(255, 206, 86, 0.2)', 'rgba(75, 192, 192, 0.2)', 'rgba(153, 102, 255, 0.2)', 'rgba(255, 159, 64, 0.2)'],
                    borderColor: ['rgba(54, 162, 235, 1)', 'rgba(255, 99, 132, 1)', 'rgba(255, 206, 86, 1)', 'rgba(75, 192, 192, 1)', 'rgba(153, 102, 255, 1)', 'rgba(255, 159, 64, 1)'],
                    borderWidth: 1
                  }]
                },
                options: {
                  title: {
                    display: true,
                    text: 'Sexo/Gênero'
                  }
                }
              });
            }

            var aStateAddress = app.groupBy(responsePayload, 'address_state');

            if (aStateAddress) {
              var dataNames = [];
              var dataset = [];

              for (var _key2 in aStateAddress) {
                if (Object.hasOwnProperty.call(aStateAddress, _key2)) {
                  var _element5 = aStateAddress[_key2];

                  if (_element5.length > 0) {
                    // console.log(key, element.length)
                    if (_key2 != 'Escolher...' && _key2 != 'undefined') {
                      dataset.push(_element5.length);
                      dataNames.push(_key2);
                    }
                  }
                }
              }

              var ctx5 = document.getElementById('fivChart').getContext('2d');
              var myChart2 = new Chart(ctx5, {
                type: 'bar',
                data: {
                  labels: dataNames,
                  datasets: [{
                    label: 'Quantidade',
                    data: dataset,
                    backgroundColor: 'rgba(54, 162, 235, 0.2)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    borderWidth: 1
                  }]
                },
                options: {
                  title: {
                    display: true,
                    text: 'Procedência'
                  },
                  scales: {
                    yAxes: [{
                      ticks: {
                        beginAtZero: true
                      }
                    }]
                  }
                }
              });
            }

            var aAgeRanged = new Array();
            aAgeRanged[0] = new Array();
            aAgeRanged[1] = new Array();
            aAgeRanged[2] = new Array();
            aAgeRanged[3] = new Array();
            aAgeRanged[4] = new Array();
            aAgeRanged[5] = new Array();
            var oTitleRange = {
              f: {
                min: 17,
                max: 24,
                range: 0
              },
              s: {
                min: 25,
                max: 34,
                range: 1
              },
              t: {
                min: 35,
                max: 44,
                range: 2
              },
              i: {
                min: 45,
                max: 54,
                range: 3
              },
              v: {
                min: 55,
                max: 64,
                range: 4
              },
              x: {
                min: 65,
                max: 99,
                range: 5
              }
            };
            responsePayload.forEach(function (u) {
              var iUserAge = app.getAge(u.birthday);

              for (var _key3 in oTitleRange) {
                if (Object.hasOwnProperty.call(oTitleRange, _key3)) {
                  var _element6 = oTitleRange[_key3];

                  if (iUserAge > _element6.min && iUserAge < _element6.max) {
                    aAgeRanged[_element6.range].push(_element6);
                  }
                }
              }
            }); // aAgeRanged//var aBirthday = app.groupBy(responsePayload, 'birthday');

            if (aAgeRanged) {
              var dataset = [];

              for (var _key4 in aAgeRanged) {
                if (Object.hasOwnProperty.call(aAgeRanged, _key4)) {
                  var _element7 = aAgeRanged[_key4];
                  dataset.push(_element7.length);
                }
              }

              var ctx3 = document.getElementById('thirdChart').getContext('2d');
              var myChart3 = new Chart(ctx3, {
                type: 'bar',
                data: {
                  labels: ["18 - 24", "25 - 34", "35 - 44", "45 - 54", " 55 - 64", "65+"],
                  datasets: [{
                    label: 'Quantidade',
                    data: dataset,
                    backgroundColor: ['rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)', 'rgba(255, 206, 86, 0.2)', 'rgba(75, 192, 192, 0.2)', 'rgba(153, 102, 255, 0.2)', 'rgba(255, 159, 64, 0.2)', 'rgba(54, 162, 235, 0.2)', 'rgba(255, 206, 86, 0.2)', 'rgba(75, 192, 192, 0.2)', 'rgba(153, 102, 255, 0.2)', 'rgba(255, 159, 64, 0.2)'],
                    borderColor: ['rgba(255, 99, 132, 1)', 'rgba(54, 162, 235, 1)', 'rgba(255, 206, 86, 1)', 'rgba(75, 192, 192, 1)', 'rgba(153, 102, 255, 1)', 'rgba(255, 159, 64, 1)', 'rgba(54, 162, 235, 1)', 'rgba(255, 206, 86, 1)', 'rgba(75, 192, 192, 1)', 'rgba(153, 102, 255, 1)', 'rgba(255, 159, 64, 1)'],
                    borderWidth: 1
                  }]
                },
                options: {
                  title: {
                    display: true,
                    text: 'Faixa etária'
                  },
                  scales: {
                    yAxes: [{
                      ticks: {
                        beginAtZero: true
                      }
                    }]
                  }
                }
              });
            }

            var aThematicalAreas = app.groupBy(responsePayload, 'thematical_areas');

            if (aThematicalAreas) {
              var dataset = [];
              var dataNames = [];

              for (var _key5 in aThematicalAreas) {
                if (Object.hasOwnProperty.call(aThematicalAreas, _key5)) {
                  var _element8 = aThematicalAreas[_key5];

                  if (_element8.length > 30) {
                    // console.log(key, element.length)
                    dataset.push(_element8.length);
                    dataNames.push(_key5);
                  }
                }
              }

              var ctx4 = document.getElementById('fourthChart').getContext('2d');
              var myChart4 = new Chart(ctx4, {
                type: 'bar',
                data: {
                  labels: dataNames,
                  datasets: [{
                    label: 'Quantidade',
                    data: dataset,
                    backgroundColor: ['rgba(26, 188, 156,1.0)', 'rgba(46, 204, 113,1.0)', 'rgba(52, 152, 219,1.0)', 'rgba(155, 89, 182,1.0)', 'rgba(52, 73, 94,1.0)', 'rgba(241, 196, 15,1.0)', 'rgba(230, 126, 34,1.0)', 'rgba(231, 76, 60,1.0)', 'rgba(236, 240, 241,1.0)', 'rgba(149, 165, 166,1.0)', 'rgba(254, 164, 127,1.0)', 'rgba(37, 204, 247,1.0)', 'rgba(234, 181, 67,1.0)', 'rgba(85, 230, 193,1.0)', 'rgba(44, 58, 71,1.0)', 'rgba(179, 55, 113,1.0)', 'rgba(253, 114, 114,1.0)', 'rgba(189, 197, 129,1.0)', 'rgba(214, 162, 232,1.0)', 'rgba(130, 88, 159,1.0)'],
                    borderColor: ['rgba(26, 188, 156,1.0)', 'rgba(46, 204, 113,1.0)', 'rgba(52, 152, 219,1.0)', 'rgba(155, 89, 182,1.0)', 'rgba(52, 73, 94,1.0)', 'rgba(241, 196, 15,1.0)', 'rgba(230, 126, 34,1.0)', 'rgba(231, 76, 60,1.0)', 'rgba(236, 240, 241,1.0)', 'rgba(149, 165, 166,1.0)', 'rgba(254, 164, 127,1.0)', 'rgba(37, 204, 247,1.0)', 'rgba(234, 181, 67,1.0)', 'rgba(85, 230, 193,1.0)', 'rgba(44, 58, 71,1.0)', 'rgba(179, 55, 113,1.0)', 'rgba(253, 114, 114,1.0)', 'rgba(189, 197, 129,1.0)', 'rgba(214, 162, 232,1.0)', 'rgba(130, 88, 159,1.0)'],
                    borderWidth: 1
                  }]
                },
                options: {
                  title: {
                    display: true,
                    text: 'Temática'
                  },
                  scales: {
                    yAxes: [{
                      ticks: {
                        beginAtZero: true
                      }
                    }]
                  }
                }
              });
            } // console.log(app.counters.dash.bruteValue)
            // console.log(app.counters.dash.liquidValue)
            // console.log(app.counters.dash.congressValue)


            document.querySelector('.brute-value').innerText = 'R$ ' + app.counters.dash.bruteValue.toLocaleString('pt-BR', {
              minimumFractionDigits: 2,
              maximumFractionDigits: 3
            });
            document.querySelector('.liquid-value').innerText = 'R$ ' + app.counters.dash.liquidValue.toLocaleString('pt-BR', {
              minimumFractionDigits: 2,
              maximumFractionDigits: 3
            });
            document.querySelector('.congress-value').innerText = 'R$ ' + app.counters.dash.congressValue.toLocaleString('pt-BR', {
              minimumFractionDigits: 2,
              maximumFractionDigits: 3
            });
            document.querySelector('.congress-brute').innerText = 'R$ ' + app.counters.dash.congressBrute.toLocaleString('pt-BR', {
              minimumFractionDigits: 2,
              maximumFractionDigits: 3
            });
            document.querySelector('.last7-value').innerText = 'R$ ' + app.counters.dash.last7Value.toLocaleString('pt-BR', {
              minimumFractionDigits: 2,
              maximumFractionDigits: 3
            });
            document.querySelector('.last30-value').innerText = 'R$ ' + app.counters.dash.last30Value.toLocaleString('pt-BR', {
              minimumFractionDigits: 2,
              maximumFractionDigits: 3
            });
          }
        });
      } else {
        window.location.reload();
      }
    }, 3000);
  }

  if (primaryClass == "accountUsers") {
    var parseExcel = function parseExcel(file) {
      // Get The File From The Input
      var oFile = file.target.files[0];
      var sFilename = oFile.name;
      var reader = new FileReader();

      reader.onload = function (e) {
        var data = e.target.result;
        var workbook = XLSX.read(data, {
          type: 'binary'
        });
        workbook.SheetNames.forEach(function (sheetName) {
          // Here is your object
          var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
          var json_object = JSON.stringify(XL_row_object);
          document.getElementById('bulkoutput').innerText = json_object; //console.log(XL_row_object);
        });
      };

      reader.onerror = function (ex) {
        console.error(ex);
      };

      reader.readAsBinaryString(oFile);
    };

    var telMask = ['(99) 9999-99999', '(99) 99999-9999'];
    var tel = document.querySelector('#phone');
    VMasker(tel).maskPattern(telMask[0]);
    tel.addEventListener('input', app.inputHandler.bind(undefined, telMask, 14), false);
    var mobile = document.querySelector('#mobile');
    VMasker(mobile).maskPattern(telMask[0]);
    mobile.addEventListener('input', app.inputHandler.bind(undefined, telMask, 14), false);
    var professional_phone = document.querySelector('#professional_phone');
    VMasker(professional_phone).maskPattern(telMask[0]);
    professional_phone.addEventListener('input', app.inputHandler.bind(undefined, telMask, 14), false);
    var docMask = ['999.999.999-999'];
    var doc = document.querySelector('#cpf');
    VMasker(doc).maskPattern(docMask[0]);
    doc.addEventListener('input', app.inputHandler.bind(undefined, docMask, 14), false);
    var docRg = ['99.999.999-999999'];
    var doc = document.querySelector('#rg');
    VMasker(doc).maskPattern(docRg[0]);
    doc.addEventListener('input', app.inputHandler.bind(undefined, docRg, 14), false); // Get the token from the current sessionToken, or log the user out if none is there

    var token = typeof app.user.token == 'string' ? app.user.token : false;

    if (token) {
      var queryStringObject = {
        'host': window.location.host,
        'access_token': token
      };
      app.listProducts(queryStringObject);
      app.client.request(undefined, endpoint + '/api/Admins/Institution/Users', 'GET', queryStringObject, undefined, function (statusCode, responsePayload) {
        if (statusCode == 200 && responsePayload) {
          // Show each created check as a new row in the table
          app.lists.user = [];
          var dataSet = [];

          for (var index = 0; index < responsePayload.length; index++) {
            var user = responsePayload[index];
            var child = [];
            var oTransaction = _typeof(user.transactions) == 'object' && user.transactions.length > 0 ? app.searchArrayThe("status", "paid", user.transactions) : false;
            var oTransactionDate = _typeof(oTransaction) == 'object' && typeof oTransaction.date == 'string' && oTransaction.date.length > 0 ? new Date(oTransaction.date) : false;
            var oTodayDate = new Date();
            child[0] = user.title || 'n/a';
            child[1] = user.name || 'n/a';
            child[2] = user.email || 'n/a';
            child[3] = user.cpf || 'n/a';
            child[4] = _typeof(oTransaction) == 'object' && oTransactionDate && oTodayDate.getFullYear() <= oTransactionDate.getFullYear() ? "<i class=\"fad fa-check-circle text-success\" title=\"Este usu\xE1rio efetuou o pagamento da anuidade.\"></i> Ok" : "<i class=\"fad fa-minus-circle text-warning\" title=\"Este usu\xE1rio ainda n\xE3o efetuou o pagamento da anuidade.\"></i> Pendente";
            child[5] = user.address_state || 'n/a';
            child[6] = "\n              <button class=\"open-modal btn btn-primary\" \n                onClick=\"app.loadUserDataModal('customer-detail-modal','".concat(user.id, "')\">\n                  Detalhes <i class=\"fad fa-info-circle\"></i>\n              </button>\n              <button class=\"open-modal btn btn-danger\" \n                onClick=\"app.loadIsentation('").concat(user.id, "')\">\n                  Isen\xE7\xF5es <i class=\"fad fa-sparkles\"></i>\n              </button>\n              <button class=\"open-modal btn btn-success\" \n                onClick=\"app.createUserPayment('").concat(user.id, "')\">\n                  Criar Pagamento <i class=\"fad fa-file-invoice-dollar\"></i>\n              </button>\n              \n              ");
            app.lists.user.push(user);
            dataSet.push(child);
          }

          if ($.fn.dataTable.isDataTable('#tableUsers')) {
            var localTable = $('#tableUsers').DataTable();
            localTable.destroy();
          }

          $('#tableUsers thead th').each(function () {
            var title = $(this).text();

            if (title != "Ações") {
              $(this).html("\n                <label style=\"display: none\">".concat(title.trim(), "</label>\n                <input type=\"text\" class=\"form-control\" placeholder=\"").concat(title.trim(), "\">\n                "));
            }
          });
          var table = $('#tableUsers').DataTable({
            scrollX: true,
            lengthChange: true,
            buttons: ['excel', 'pdf'],
            data: dataSet,
            columnDefs: [],
            order: [[1, "asc"]],
            language: {
              lengthMenu: "Exibindo _MENU_  ",
              zeroRecords: "Nenhum registro disponível.",
              info: "Exibindo pagina _PAGE_ de _PAGES_",
              search: "Filtro _INPUT_",
              paginate: {
                "next": ">",
                "previous": "<"
              }
            }
          });
          table.columns().every(function () {
            var that = this;
            $('input', this.header()).on('keyup change', function () {
              if (that.search() !== this.value) {
                that.search(this.value).draw();
              }
            });
          });
          setTimeout(function () {
            window.dispatchEvent(new Event('resize'));
          }, 1000);
          $('.page-hover').fadeOut();
        }
      });
    } else {
      app.logUserOut();
    }

    var oFileIn;
    oFileIn = document.getElementById('bulk');

    if (oFileIn.addEventListener) {
      oFileIn.addEventListener('change', parseExcel, false);
    }

    ;
  }

  if (primaryClass == "accountEvents") {
    app.uploadFile("file-upload-top-congress", "topImageUrl", "topImageDisplay");
    app.uploadFile("file-upload-certificate-congress", "certImageUrl", "certImageDisplay");
    app.loadEventsPage(function (response) {
      console.log(response);
    });
  }

  if (primaryClass == "accountFinancial") {
    window.imageTargetEvent = app.getBase64Image(document.getElementById("imageid")); // Get the token from the current sessionToken, or log the user out if none is there

    var token = typeof app.user.token == 'string' ? app.user.token : false;

    if (token) {
      // Fetch the user data
      var queryStringObject = {
        'host': window.location.host,
        'access_token': token
      };
      var regionsByState = {
        "AM": "Norte",
        "RR": "Norte",
        "AP": "Norte",
        "PA": "Norte",
        "TO": "Norte",
        "RO": "Norte",
        "AC": "Norte",
        "MA": "Nordeste",
        "PI": "Nordeste",
        "CE": "Nordeste",
        "RN": "Nordeste",
        "PE": "Nordeste",
        "PB": "Nordeste",
        "SE": "Nordeste",
        "AL": "Nordeste",
        "BA": "Nordeste",
        "MT": "Centro-Oeste",
        "MS": "Centro-Oeste",
        "GO": "Centro-Oeste",
        "DF": "Centro-Oeste",
        "SP": "Sudeste",
        "RJ": "Sudeste",
        "ES": "Sudeste",
        "MG": "Sudeste",
        "PR": "Sul",
        "SC": "Sul",
        "RS": "Sul"
      };
      var numberPattern = /\d+/g;
      app.client.request(undefined, endpoint + '/api/Admins/Institution/Transactions', 'GET', queryStringObject, undefined, function (statusCode, responsePayload) {
        if (statusCode == 200 && responsePayload) {
          window.eventTargetName = 'Intercom ERP';
          var oType = {
            credit_card: "Cartão de Crédito",
            invoice: "Boleto",
            boleto: "Boleto",
            platform_include: "Indefinido"
          };
          var oStatus = {
            paid: "Pago",
            processing: "Processando",
            wait: "Aguardando",
            waiting: "Aguardando",
            waiting_payment: "Aguardando",
            refunded: "Estornado",
            chargeback: "Chargeback"
          }; // Show each created check as a new row in the table

          var dataSet = [];
          app.lists.user = [];

          for (var index = 0; index < responsePayload.length; index++) {
            var element = responsePayload[index]; // console.log(element)

            var oCustomer = {};

            for (var key in element.customer) {
              if (Object.hasOwnProperty.call(element.customer, key)) {
                var value = element.customer[key];
                oCustomer[key.trim()] = value;
              }
            }

            element.customer = oCustomer;
            console.log(element);
            var child = [];
            child[0] = oCustomer.associated_number || 'n/a';
            child[1] = oCustomer.name || 'n/a';
            child[2] = oCustomer.email || 'n/a';
            child[3] = "<span style=\"display: none;\">".concat(new Date(element.date).getTime(), "</span> ").concat(new Date(element.date).toLocaleDateString());
            child[4] = typeof oCustomer.cpf == 'string' && oCustomer.cpf.length > 0 ? oCustomer.cpf.match(numberPattern).join('') : 'n/a';
            child[5] = "".concat((element.amount / 100).toLocaleString({
              minimumFractionDigits: 2,
              maximumFractionDigits: 2
            }));
            child[6] = "".concat((element.amount * 0.06 / 100).toLocaleString({
              minimumFractionDigits: 2,
              maximumFractionDigits: 2
            }).replace(',', '.'));
            child[7] = element.type == 'platform_include' && typeof element.madeBy == 'string' ? 'Isento' : oStatus[element.status];
            child[8] = element.ref || 'n/a';
            child[9] = typeof oCustomer['address_state'] == 'string' && oCustomer['address_state'].length > 0 && oCustomer['address_state'] != 'EX' && oCustomer['address_state'] != 'Escolher...' ? regionsByState[oCustomer['address_state'].toUpperCase()] : "n/a";
            child[10] = typeof element.link == "string" ? element.link : "n/a";
            child[11] = "<button class=\"btn btn-primary\" onClick=\"app.loadUserDataModal('customer-detail-modal','".concat(oCustomer.id, "')\"><i class=\"fad fa-file-search\"></i> Detalhes</button>"); //TIPO `${element.type == 'boleto' && element.status == 'waiting_payment' ? '<a href="'+element.tx.boleto_url+'" target="_blank" class"float-left"><i class="fad fa-file-pdf"></i> '+oType[element.type]+'</a>' : oType[element.type] }`;

            if (element.ref != "Pagamento: Pagamento histórico") {
              app.lists.user.push(oCustomer);
              dataSet.push(child);
            }
          }

          var tableTx = $('#tableTransactions').DataTable({
            lengthChange: true,
            buttons: [{
              extend: 'pdfHtml5',
              filename: "Relat\xF3rio Financeiro Intercom ERP - ".concat(new Date().getUTCDate()),
              orientation: 'portrait',
              pageSize: 'A4',
              exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
              },
              customize: function customize(doc) {
                doc.content[0].text = '';
                var now = new Date();
                var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
                doc.pageMargins = [20, 60, 20, 30]; // Set the font size fot the entire document

                doc.defaultStyle.fontSize = 7; // Set the fontsize for the table header

                doc.styles.tableHeader.fontSize = 7;

                doc['header'] = function () {
                  return {
                    columns: [{
                      image: window.imageTargetEvent,
                      width: 36
                    }, {
                      alignment: 'left',
                      italics: true,
                      text: window.eventTargetName,
                      fontSize: 15,
                      margin: [0, 0]
                    }, {
                      alignment: 'right',
                      fontSize: 12,
                      text: 'Pagamentos realizados na plataforma UHUB'
                    }],
                    margin: 20
                  };
                };

                doc['footer'] = function (page, pages) {
                  return {
                    columns: [{
                      alignment: 'left',
                      text: ['Gerado em: ', {
                        text: jsDate.toString()
                      }]
                    }, {
                      alignment: 'right',
                      text: ['page ', {
                        text: page.toString()
                      }, ' of ', {
                        text: pages.toString()
                      }]
                    }],
                    margin: 20
                  };
                };

                doc.content[1].table.widths = ['20%', '20%', '10%', '15%', '30%', '5%'];
                var objLayout = {};

                objLayout['hLineWidth'] = function (i) {
                  return .5;
                };

                objLayout['vLineWidth'] = function (i) {
                  return .5;
                };

                objLayout['hLineColor'] = function (i) {
                  return '#aaa';
                };

                objLayout['vLineColor'] = function (i) {
                  return '#aaa';
                };

                objLayout['paddingLeft'] = function (i) {
                  return 4;
                };

                objLayout['paddingRight'] = function (i) {
                  return 4;
                };

                doc.content[0].layout = objLayout;
              }
            }, 'excel'],
            data: dataSet,
            columns: [{
              title: "Nº Sócio"
            }, {
              title: "Nome"
            }, {
              title: "Email"
            }, {
              title: "Data"
            }, {
              title: "CPF"
            }, {
              title: "Bruto"
            }, {
              title: "Taxa"
            }, {
              title: "Status"
            }, {
              title: "Referência"
            }, {
              title: "Região"
            }, {
              title: "Tipo"
            }, {
              title: "Ações"
            }],
            columnDefs: [{
              "targets": [0, 1, 2, 10],
              "visible": false,
              "searchable": false
            }],
            "order": [[3, "desc"]],
            "language": {
              "lengthMenu": "Exibindo _MENU_  ",
              "zeroRecords": "Nenhum registro disponível.",
              "info": "Exibindo pagina _PAGE_ de _PAGES_",
              "search": "Filtro _INPUT_",
              "paginate": {
                "next": ">",
                "previous": "<"
              }
            },
            "footerCallback": function footerCallback(row, data, start, end, display) {
              var api = this.api(),
                  data; // // Remove the formatting to get integer data for summation
              // var intVal = function ( i ) {
              //     var response = typeof(i) == 'string' ? Number(i.replace(/[^0-9.-]+/g,"")) : 
              //         typeof(i) == 'number' ? i : 0;
              //     return response;
              // };
              // // Total over all pages
              // var total = api
              //     .column( 3 )
              //     .data()
              //     .reduce( function (a, b) {
              //         return intVal(a) + intVal(b);
              //     }, 0 );
              // Total over this page
              // var pageTotal = api
              //     .column( 5, { page: 'current'} )
              //     .data()
              //     .reduce( function (a, b) {
              //         return intVal(a) + intVal(b);
              //     }, 0 );
              // // Update footer
              // $( api.column( 10 ).footer() ).html(
              //     `R$ ${(pageTotal / 100 ).toLocaleString('pt-BR', {
              //         minimumFractionDigits: 2,
              //         maximumFractionDigits: 3
              //       })} `
              // );
            }
          });
          tableTx.buttons().container().appendTo('#tableTransactions_wrapper .col-md-6:eq(0)');
          setTimeout(function () {
            window.dispatchEvent(new Event('resize'));
          }, 1000);
          $('.page-hover').fadeOut();
        }
      });
      app.client.request(undefined, endpoint + '/api/Admins/Institution/Products', 'GET', queryStringObject, undefined, function (statusCode, responsePayload) {
        if (statusCode == 200 && responsePayload) {
          app.products = responsePayload; // Show each created check as a new row in the table

          var dataSet = [];

          for (var index = 0; index < responsePayload.length; index++) {
            var oProduct = responsePayload[index];
            var child = [];
            child[0] = oProduct.name;
            child[1] = "R$ ".concat((oProduct.price / 100).toLocaleString('pt-BR', {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2
            }));
            child[2] = new Date(oProduct.date).toLocaleString();
            child[3] = oProduct.duration;
            child[4] = typeof oProduct["default"] == 'boolean' && oProduct["default"] ? "<span class=\"text-success\">Ativo</span>" : '-';
            child[5] = "<button class=\"btn btn-primary\" onClick=\"app.editProduct('".concat(oProduct.id, "')\"><i class=\"fad fa-edit\"></i> Editar</button> <button class=\"btn btn-danger\" onClick=\"app.delete('").concat(oProduct.id, "','Product')\"><i class=\"fad fa-trash\"></i> Deletar</div>");
            dataSet.push(child);
          }

          var table = $('#tableProducts').DataTable({
            lengthChange: true,
            buttons: ['excel', 'pdf'],
            data: dataSet,
            columns: [{
              title: "Nome"
            }, {
              title: "Preço"
            }, {
              title: "Data (início da validade)"
            }, {
              title: "Duração (meses)"
            }, {
              title: "Status"
            }, {
              title: "Ações"
            }],
            "order": [[4, "desc"]],
            "language": {
              "lengthMenu": "Exibindo _MENU_  ",
              "zeroRecords": "Nenhum registro disponível.",
              "info": "Exibindo pagina _PAGE_ de _PAGES_",
              "search": "Filtro _INPUT_",
              "paginate": {
                "next": ">",
                "previous": "<"
              }
            }
          });
          table.buttons().container().appendTo('#tableProducts_wrapper .col-md-6:eq(0)');
          setTimeout(function () {
            window.dispatchEvent(new Event('resize'));
          }, 1000);
        }
      });
      document.querySelector('.process-payments').addEventListener('click', function (e) {
        $('.page-hover').fadeIn(1000);
        setTimeout(function () {
          app.client.request(undefined, endpoint + '/api/Admins/Institution/Transactions/Process', 'GET', queryStringObject, undefined, function (statusCode, responsePayload) {
            if (statusCode == 200 && responsePayload) {
              Swal.fire('Uhuu', 'Sincronização efetuada com sucesso', 'success').then(function (e) {
                window.location.reload();
              });
            }
          });
        }, 500);
      });
    } else {
      app.logUserOut();
    }
  }

  if (primaryClass == "accountConfiguration") {
    // Get the token from the current sessionToken, or log the user out if none is there
    var token = typeof app.user.token == 'string' ? app.user.token : false;

    if (token) {
      // Fetch the user data
      var queryStringObject = {
        'host': window.location.host,
        'access_token': token
      };
      app.client.request(undefined, endpoint + '/api/Admins/Institution', 'GET', queryStringObject, undefined, function (statusCode, responsePayload) {
        if (statusCode == 200 && responsePayload) {
          document.querySelector('#name').value = responsePayload.name;
          document.querySelector('#logo').value = responsePayload.logo;
          document.querySelector('#contact_email').value = responsePayload.contact_email;
          document.querySelector('#contact_phone').value = responsePayload.contact_phone;
          document.querySelector('#logomark').value = responsePayload.logomark;
          $('.page-hover').fadeOut();
        }
      });
    }
  }
}; // Authentication, Validatation and core sutff.


app.validateFrbAuth = function () {
  var displayName = app.user.displayName;
  var email = app.user.email;
  var emailVerified = app.user.emailVerified;
  var photoURL = app.user.photoURL;
  var uid = app.user.uid;
  var phoneNumber = app.user.phoneNumber;
  var providerData = app.user.providerData;
  app.user.getIdToken().then(function (accessToken) {
    var requestOb = {
      name: displayName,
      email: email,
      emailVerified: emailVerified,
      phoneNumber: phoneNumber,
      picture: photoURL,
      uid: uid,
      accessToken: accessToken,
      host: window.location.host
    };
    app.client.request(undefined, endpoint + '/api/Admins/authenticate', 'POST', undefined, requestOb, function (statusCode, responsePayload) {
      if (statusCode == 200) {
        app.setData('token', responsePayload.id);
        app.setData('client', responsePayload.clientId);
        app.user.token = responsePayload.id;
        app.user.client = responsePayload.clientId;
      } else {
        window.location = '/?rsn=not-valid-authentication';
      }
    });
  });
};

app.checkAuthentication = function () {
  var isAuthenticated = app.getData('token');
  var hasClientId = app.getData('client');
  if (!isAuthenticated || !hasClientId) app.validateFrbAuth();
  app.user.token = isAuthenticated;
  return true;
};

app.loadConfiguration = function () {
  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyCCjMIuaOfDNygqU1rGOOyA8h8mST07taE",
    authDomain: "uhubclub.firebaseapp.com",
    databaseURL: "https://uhubclub-default-rtdb.firebaseio.com",
    projectId: "uhubclub",
    storageBucket: "uhubclub.appspot.com",
    messagingSenderId: "1090303585021",
    appId: "1:1090303585021:web:a44056d54ce3ce099dac94",
    measurementId: "G-0500E63LTN"
  }; // Initialize Firebase

  firebase.initializeApp(firebaseConfig); // Set analytics to collect data from application

  firebase.analytics();
};

app.initTranslate = function () {
  setTimeout(function () {
    var dl = 'pt-BR';
    var bl = navigator.language;
    var google = 'Google';
    var facebook = 'Facebook';
    var email = 'password';
    bl === 'en-US' || bl === 'en-us' ? runlang(bl) : runlang(dl);

    function translate(text, provider) {
      var container = $('.firebaseui-idp-' + provider.toLowerCase() + ' .firebaseui-idp-text-long');

      if (provider === 'password') {
        provider = 'email';
        container.text(text + ' ' + provider);
      } else {
        container.text(text + ' ' + provider);
      }
    }

    function translate2(html, provider) {
      var container = $('.firebaseui-card-footer.firebaseui-provider-sign-in-footer');
      container.html(html);
    }

    function runlang(lang) {
      if (lang === 'en-US' || lang === 'en-us') {
        var sign_in = 'Sign in with';
        translate(sign_in, google);
        translate(sign_in, facebook);
        translate(sign_in, email);
      } else {
        var sign_in = 'Entrar com';
        translate(sign_in, google);
        translate(sign_in, facebook);
        translate(sign_in, email);
        translate2("<p class=\"firebaseui-tos firebaseui-tospp-full-message\">Ao continuar, voc\xEA indica que aceita nossos <a href=\"/legal/terms-of-use.html\" class=\"firebaseui-link firebaseui-tos-link\" target=\"_blank\">Termos de Servi\xE7o</a> e <a href=\"/legal/privacy-policy.html\" class=\"firebaseui-link firebaseui-pp-link\" target=\"_blank\">Pol\xEDtica de Privacidade</a></p>", '.firebaseui-provider-sign-in-footer');
      }
    }
  }, 1500);
};

app.startAuthentication = function () {
  // FirebaseUI config.
  var uiConfig = {
    signInSuccessUrl: 'pages/account-dashboard.html',
    signInOptions: [// Leave the lines as is for the providers you want to offer your users.
    firebase.auth.GoogleAuthProvider.PROVIDER_ID, firebase.auth.EmailAuthProvider.PROVIDER_ID],
    // tosUrl and privacyPolicyUrl accept either url string or a callback
    // function.
    // Terms of service url/callback.
    tosUrl: 'legal/terms-of-use.html',
    // Privacy policy url/callback.
    privacyPolicyUrl: function privacyPolicyUrl() {
      window.location.assign('legal/privacy-policy.html');
    }
  }; // Initialize the FirebaseUI Widget using Firebase.

  var ui = new firebaseui.auth.AuthUI(firebase.auth()); // The start method will wait until the DOM is loaded.

  ui.start('#firebaseui-auth-container', uiConfig);
  app.initTranslate();
};

app.checkIsLoggedIn = function () {
  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      app.user = {};
      app.user = user;
      app.checkAuthentication();
      app.loadByClass();
    } else {
      app.logUserOut();
    }
  }, function (error) {
    console.log(error);
  });
};

app.loadPageOnData = function () {
  // Get the current page from the body class
  var bodyClasses = document.querySelector("body").classList;
  var primaryClass = typeof bodyClasses[0] == 'string' ? bodyClasses[0] : false;

  if (primaryClass == 'index') {
    localStorage.removeItem('token');
    localStorage.removeItem('client');

    if (_typeof(app.user) == 'object' && typeof app.user.token == 'string' && app.user.token.length > 0) {
      delete app.user.token;
    }

    app.startAuthentication();
  }

  if (primaryClass != 'index') {
    app.checkIsLoggedIn();
  }
}; // AJAX Client (for RESTful API)


app.client = {}; // Interface for making API calls

app.client.request = function (headers, path, method, queryStringObject, payload, callback) {
  // Set defaults
  headers = _typeof(headers) == 'object' && headers !== null ? headers : {};
  path = typeof path == 'string' ? path : '/';
  method = typeof method == 'string' && ['POST', 'GET', 'PUT', 'DELETE'].indexOf(method.toUpperCase()) > -1 ? method.toUpperCase() : 'GET';
  queryStringObject = _typeof(queryStringObject) == 'object' && queryStringObject !== null ? queryStringObject : {};
  payload = _typeof(payload) == 'object' && payload !== null ? payload : {};
  callback = typeof callback == 'function' ? callback : false; // For each query string parameter sent, add it to the path

  var requestUrl = path + '?';
  var counter = 0;

  for (var queryKey in queryStringObject) {
    if (queryStringObject.hasOwnProperty(queryKey)) {
      counter++; // If at least one query string parameter has already been added, preprend new ones with an ampersand

      if (counter > 1) {
        requestUrl += '&';
      } // Add the key and value


      requestUrl += queryKey + '=' + queryStringObject[queryKey];
    }
  } // Form the http request as a JSON type


  var xhr = new XMLHttpRequest();
  xhr.open(method, requestUrl, true);
  xhr.setRequestHeader("Content-type", "application/json"); // For each header sent, add it to the request

  for (var headerKey in headers) {
    if (headers.hasOwnProperty(headerKey)) {
      xhr.setRequestHeader(headerKey, headers[headerKey]);
    }
  } // If there is a current session token set, add that as a header


  if (app.session.token) {
    xhr.setRequestHeader("acces_token", app.session.token);
  } // When the request comes back, handle the response


  xhr.onreadystatechange = function () {
    if (xhr.readyState == XMLHttpRequest.DONE) {
      var statusCode = xhr.status;
      var responseReturned = xhr.responseText; // Callback if requested

      if (callback) {
        try {
          var parsedResponse = JSON.parse(responseReturned);
          callback(statusCode, parsedResponse);
        } catch (e) {
          callback(statusCode, false);
        }
      }
    }
  }; // Send the payload as JSON


  var payloadString = JSON.stringify(payload);
  xhr.send(payloadString);
}; // Bind the forms


app.bindForms = function () {
  if (document.querySelector("form")) {
    var allForms = document.querySelectorAll("form");

    for (var i = 0; i < allForms.length; i++) {
      allForms[i].addEventListener("submit", function (e) {
        // Stop it from submitting
        e.preventDefault();
        var formId = this.id;
        var path = this.action;
        var method = this.method.toUpperCase(); // // Hide the error message (if it's currently shown due to a previous error)

        document.querySelector("#" + formId + " .formError").style.display = 'none'; // Set the host always into forms for admin recognize from where its the request

        document.querySelector("#" + formId + " #eventHost").value = window.location.host; // Hide the success message (if it's currently shown due to a previous error)

        if (document.querySelector("#" + formId + " .formSuccess")) {
          document.querySelector("#" + formId + " .formSuccess").style.display = 'none';
        }

        if (formId == 'updateEvent') {
          if (_typeof(app.ckeditor.resumeCongress) == 'object') {
            document.getElementById('resumeCongress').innerText = app.ckeditor.resumeCongress.getData();
          }
        } // Turn the inputs into a payload


        var payload = {};
        var elements = this.elements;

        for (var i = 0; i < elements.length; i++) {
          if (elements[i].type !== 'submit') {
            // Determine class of element and set value accordingly
            var classOfElement = typeof elements[i].classList.value == 'string' && elements[i].classList.value.length > 0 ? elements[i].classList.value : '';
            var valueOfElement = elements[i].type == 'checkbox' && classOfElement.indexOf('multiselect') == -1 ? elements[i].checked : classOfElement.indexOf('intval') == -1 ? elements[i].value : parseInt(elements[i].value);
            var elementIsChecked = elements[i].checked; // Override the method of the form if the input's name is _method

            var nameOfElement = elements[i].name;

            if (nameOfElement == '_method') {
              method = valueOfElement;
            } else {
              // Create an payload field named "method" if the elements name is actually httpmethod
              if (nameOfElement == 'httpmethod') {
                nameOfElement = 'method';
              } // Create an payload field named "id" if the elements name is actually uid


              if (nameOfElement == 'uid') {
                nameOfElement = 'id';
              } // If the element has the class "multiselect" add its value(s) as array elements


              if (classOfElement.indexOf('multiselect') > -1) {
                if (elementIsChecked) {
                  payload[nameOfElement] = _typeof(payload[nameOfElement]) == 'object' && payload[nameOfElement] instanceof Array ? payload[nameOfElement] : [];
                  payload[nameOfElement].push(valueOfElement);
                }
              } else {
                payload[nameOfElement] = valueOfElement;
              }
            }
          }
        } // If the method is DELETE, the payload should be a queryStringObject instead


        var queryStringObject = method == 'DELETE' ? payload : {};

        if (app.user.token) {
          queryStringObject['access_token'] = app.user.token;
        }

        if (formId == 'staff-stand-invite') {
          console.log(new Date().toISOString());
          document.querySelector("#" + formId + " .btn").style.display = 'none';
          $('#setPreference').fadeIn(1000);
        } // Call the API


        app.client.request(undefined, path, method, queryStringObject, payload, function (statusCode, responsePayload) {
          // Display an error on the form if needed
          if (statusCode !== 200) {
            if (statusCode == 403) {
              // log the user out
              app.logUserOut();
            } else {
              // Try to get the error from the api, or set a default error message
              var error = typeof responsePayload.Error == 'string' ? responsePayload.Error : 'Houve um erro, por favor tente novamente, caso o erro persista entre em contato com o suporte técnigo: contato@uhub.team.'; // Set the formError field with the error text

              document.querySelector("#" + formId + " .formError").innerHTML = error; // Show (unhide) the form error field on the form

              document.querySelector("#" + formId + " .formError").style.display = 'block';
            }
          } else {
            // If successful, send to form response processor
            app.formResponseProcessor(formId, payload, responsePayload);
          }
        });
      });
    }
  }
};

app.formResponseProcessor = function (formId, payload, responsePayload) {
  document.querySelector("#" + formId + " .formSuccess").style.display = 'block';

  if (formId == 'editProduct') {
    Swal.fire('Uhu', 'O seu produto foi alterado com sucesso.', 'success').then(function (e) {
      if (e.value) {
        window.location.reload();
      }
    });
  }

  if (formId == 'createNewEvent') {
    Swal.fire('Uhu', 'O seu evento foi criado com sucesso.', 'success').then(function (e) {
      if (e.value) {
        window.location.reload();
      }
    });
  }

  if (formId == 'updateEvent') {
    Swal.fire('Uhu', 'O seu evento foi atualizado com sucesso.', 'success').then(function (e) {
      if (e.value) {
        window.location.reload();
      }
    });
  }

  if (formId == 'inscriptionWork') {
    Swal.fire('Uhu', 'A indicação foi atualizada com sucesso.', 'success').then(function (e) {
      if (e.value) {
        $("#".concat(formId, "Modal")).modal('hide');
      }
    });
  }

  if (formId == 'updateCustomerDataInformation') {
    Swal.fire('Uhu', 'O Usuário foi alterado com sucesso.', 'success').then(function (e) {
      if (e.value) {
        window.location.reload();
      }
    });
  }
};

app.checkDarkTheme = function (data) {
  console.log("Checking Theme: ".concat(data.toLocaleString()));
  var hour = data.getHours();

  if (hour > 18 || hour < 6) {
    document.body.className = document.body.className.replace("bg-light", "bg-dark");

    if (document.querySelector('.page-hover')) {
      document.querySelector('.page-hover').classList.add('bg-dark');
    }

    if (document.querySelector('.jumbotron.pt-3.pb-1 > .lead')) {
      document.querySelector('.jumbotron.pt-3.pb-1 > .lead').classList.add('text-dark');
    }

    if (document.querySelector('.modal-content')) {
      document.querySelector('.modal-content').classList.add('bg-dark');
    }

    app.addNewStyle("\n      .modal-content {\n        background: #2a2d2e;\n      }\n      .modal-content label {\n        color: black!important;\n      }\n     \n    ");
    setTimeout(function () {
      // var divsElements = document.querySelectorAll('div');
      // for (let index = 0; index < divsElements.length; index++) {
      //   const element = divsElements[index];
      //   element.classList.add("bg-dark");
      //   element.classList.add("text-light");
      //   console.log(element)
      // }
      var tables = document.querySelectorAll('table');

      for (var index = 0; index < tables.length; index++) {
        var element = tables[index];
        element.classList.add("table-dark");
      } // var labels = document.querySelectorAll('label');
      // for (let index = 0; index < labels.length; index++) {
      //   const element = labels[index];
      //   element.classList.add("bg-dark-force");
      // }


      document.querySelectorAll('label').forEach(function (e) {
        e.style.color = '#3c3c3c';
      });
    }, 3000);
  }

  return data;
};

app.start = function () {
  app.checkDarkTheme(new Date());
  app.loadConfiguration(); // Load informations to be used by page class

  app.loadPageOnData(); // Bind all form submissions

  app.bindForms();
};

app.start(); // Log the user out then redirect them

app.logUserOut = function () {
  localStorage.removeItem('token');
  app.user.token = undefined;
  window.location = '/';
}; // Helpers


app.setData = function (key, value) {
  localStorage.setItem(key, value);
  return true;
};

app.getData = function (key) {
  return localStorage.getItem(key);
};

app.getBase64Image = function (img) {
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
  var dataURL = canvas.toDataURL("image/png");
  return dataURL;
};

app.searchArrayThe = function (nameKey, nameValue, myArray) {
  var client = app.getData('client');
  var now = new Date();
  myArray.reverse();

  for (var i = 0; i < myArray.length; i++) {
    var arrDate = new Date(myArray[i].date);

    if (myArray[i][nameKey] === nameValue && myArray[i].clientId == client && arrDate.getFullYear() == now.getFullYear() && myArray[i].link == 'membership') {
      return myArray[i];
    }
  }
};

app.searchArray = function (nameKey, nameValue, myArray) {
  var client = app.getData('client');
  myArray.reverse();

  for (var i = 0; i < myArray.length; i++) {
    if (myArray[i][nameKey] === nameValue && myArray[i].clientId == client) {
      return myArray[i];
    }
  }
};

app.groupBy = function (xs, key) {
  return xs.reduce(function (rv, x) {
    (rv[x[key]] = rv[x[key]] || []).push(x);
    return rv;
  }, {});
};

app.handleAssociated = function (data) {
  var now = new Date();
  var key = 'transactions';
  var paid = 0;
  var unpaid = 0;
  var last7 = new Date().setDate(new Date().getDate() - 7);
  var last30 = new Date().setMonth(new Date().getMonth() - 1);

  for (var index = 0; index < data.length; index++) {
    var oUser = data[index];
    oUser.transactions.filter(function (oTransaction) {
      var oTxDate = new Date(oTransaction.date);

      if (oTransaction.status == 'paid' && oTxDate.getFullYear() >= now.getFullYear() && oTransaction.link == 'membership') {
        paid++;
        app.counters.dash.bruteValue += oTransaction.amount / 100;
        app.counters.dash.liquidValue += oTransaction.amount / 100 * 0.94;

        if (oTxDate > last7) {
          app.counters.dash.last7Value += oTransaction.amount / 100 * 0.94;
        }

        if (oTxDate > last30) {
          app.counters.dash.last30Value += oTransaction.amount / 100 * 0.94;
        }
      } else {
        unpaid++;
      }
    }, {});
  }

  return {
    paid: paid,
    unpaid: unpaid
  };
};

app.getPayByMont = function (data) {
  var now = new Date();
  var months = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  var key = 'transactions';
  var paid = 0;
  var unpaid = 0;

  for (var index = 0; index < data.length; index++) {
    var oUser = data[index];
    oUser.transactions.filter(function (oTransaction) {
      var oTxDate = new Date(oTransaction.date);

      if ((oTransaction.type == 'credit_card' || oTransaction.type == 'boleto' || oTransaction.type == 'pix') && oTransaction.status == 'paid' && now.getFullYear() == oTxDate.getFullYear() && (oTransaction.link == 'membership' || oTransaction.type == 'inscription')) {
        months[oTxDate.getMonth()] = (+months[oTxDate.getMonth()] + oTransaction.amount / 100 * 0.94).toFixed(2);
      } else if (oTransaction.link == 'inscription' || oTransaction.link.indexOf('ngresso') > -1) {
        app.counters.dash.congressValue += oTransaction.amount / 100 * 0.94;
        app.counters.dash.congressBrute += oTransaction.amount / 100;
      }
    }, {});
  }

  return months;
};

app.getAge = function (dateString) {
  var today = new Date();
  var birthDate = new Date(dateString);
  var age = today.getFullYear() - birthDate.getFullYear();
  var m = today.getMonth() - birthDate.getMonth();

  if (m < 0 || m === 0 && today.getDate() < birthDate.getDate()) {
    age--;
  }

  return age;
};

app.inputHandler = function (masks, max, event) {
  var c = event.target;
  var v = c.value.replace(/\D/g, '');
  var m = c.value.length > max ? 1 : 0;
  VMasker(c).unMask();
  VMasker(c).maskPattern(masks[m]);
  c.value = VMasker.toPattern(v, masks[m]);
};

app.validateCPF = function (strCPF) {
  var Soma = 0;
  var Resto;

  if (strCPF == "00000000000") {
    return false;
  }

  ;

  for (var i = 1; i <= 9; i++) {
    Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
  }

  ;
  Resto = Soma * 10 % 11;

  if (Resto == 10 || Resto == 11) {
    Resto = 0;
  }

  ;

  if (Resto != parseInt(strCPF.substring(9, 10))) {
    return false;
  }

  ;
  Soma = 0;

  for (var i = 1; i <= 10; i++) {
    Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
  }

  ;
  Resto = Soma * 10 % 11;

  if (Resto == 10 || Resto == 11) {
    Resto = 0;
  }

  ;

  if (Resto != parseInt(strCPF.substring(10, 11))) {
    return false;
  }

  ;
  return true;
};